﻿<%@ WebHandler Language="C#" Class="TaxbenHandler" %>

using System;
using System.Web;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;

public class TaxbenHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    private static System.Web.Script.Serialization.JavaScriptSerializer _jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
    private TaxbenOptions _options;
    private HttpContext _context;
    private string _guid;

    public void ProcessRequest(HttpContext context)
    {
        TaxbenCommandManager command = null;
        try
        {
            _context = context;
            context.Response.ContentType = "application/json";
            string serTaxbenOptions = context.Request.Form["options"];

            _options = _jsonSerializer.Deserialize<TaxbenOptions>(serTaxbenOptions);

            command = new TaxbenCommandManager(_options, _options.Guid);
            _guid = command.Id;
            command.RunCommand(WriteResponse);
        }
        catch (System.Exception ex)
        {
            var result = new TaxbenResult<double[]>()
            {
                Guid = _guid,
                ExceptionMessage = string.Concat(ex.Message, ex.InnerException != null ? string.Concat("\n(", ex.InnerException.Message, ")") : string.Empty)
            };

            context.Response.Write(Serialize(result));
        }
    }

    private void WriteResponse(string folderURL, string cmd)
    {
        try
        {
            if (folderURL == null)
                throw new System.Exception("Target folder error");

            var result = StataCsvOutputProcessor.GetOuputData(
                _options,
                _guid,
                cmd,
                folderURL,
                _context.Request.MapPath(folderURL)
            );

            _context.Response.Write(Serialize(result));

        }
        catch (System.Exception ex)
        {
            var result = new TaxbenResult<double[]>()
            {
                Guid = _guid,
                ExceptionMessage = string.Concat(ex.Message, ex.InnerException != null ? string.Concat("\n(", ex.InnerException.Message, ")") : string.Empty)
            };
            _context.Response.Write(Serialize(result));
        }
    }

    public static string Serialize(ITaxbenResult result)
    {
        return _jsonSerializer.Serialize(result);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}

/// <summary>
/// Summary description for TaxbenCommandManager
/// </summary>
public class TaxbenCommandManager
{
    private readonly string STATA_EXE = ConfigurationManager.AppSettings["StataExe"];
    private readonly string STATA_PROCESS = ConfigurationManager.AppSettings["StataProcessName"];
    private readonly int STATA_PROCESS_MAX = int.Parse(ConfigurationManager.AppSettings["StataProcessesMaxRunning"]);
    private readonly string TARGET_FOLDER = ConfigurationManager.AppSettings["TaxbenTargetFolder"];
    private readonly int TIME_OUT = int.Parse(ConfigurationManager.AppSettings["ExeTimeOutInSeconds"]);
    private readonly int RETRY = int.Parse(ConfigurationManager.AppSettings["ExeRetryNTimes"]);
    private readonly int DELETE_AFTER = int.Parse(ConfigurationManager.AppSettings["TempFileDeletionInHours"]);

    private Process _process;
    private TaxbenOptions _options;
    private Guid _id;
    public string Id
    {
        get
        {
            return _id.ToString();
        }
    }
    private string _folderURL;

    private string TargetPath
    {
        get
        {
            return HttpContext.Current.Request.MapPath(TargetURL);
        }
    }
    public string TargetURL
    {
        get
        {
            if (_folderURL == null)
            {
                _folderURL = string.Concat(TARGET_FOLDER, _id.ToString());
            }
            return _folderURL;
        }
    }


    public TaxbenCommandManager(TaxbenOptions options, string guid)
    {
        try
        {
            _options = options;
            if (!string.IsNullOrEmpty(guid))
            {
                string path = HttpContext.Current.Request.MapPath(string.Concat(TARGET_FOLDER, guid));
                if (Directory.Exists(path))
                {
                    DirectoryInfo di = new DirectoryInfo(path);
                    foreach (FileInfo fi in di.GetFiles())
                    {
                        fi.Delete();
                    }
                }
                _id = new Guid(guid);
            }
            else _id = Guid.NewGuid();
        }
        catch
        {
            throw new Exception("Error initiating command");
        }

        try
        {
            Action<string> DelTemp = DeleteTempFolders;
            DelTemp.BeginInvoke(TargetPath, ar => DelTemp.EndInvoke(ar), null);
        }
        catch { }
    }

    private void RenewProcess()
    {
        try
        {
            if (_process != null)
            {
                if (!_process.HasExited)
                {
                    _process.Kill();
                }
                _process.Dispose();
                System.Threading.Thread.Sleep(200);
            }

            Process[] running = Process.GetProcessesByName(STATA_PROCESS).Where(p => !p.HasExited).ToArray();
            while (running.Length >= STATA_PROCESS_MAX)
            {
                System.Threading.Thread.Sleep(200);

                foreach (Process process in running)
                {
                    if (process.HasExited) continue;
                    if (DateTime.Now - process.StartTime > TimeSpan.FromSeconds(TIME_OUT * RETRY))
                    {
                        try
                        {
                            process.Kill();
                            process.Dispose();
                        }
                        catch { }
                    }
                }
                running = Process.GetProcessesByName(STATA_PROCESS).Where(p => !p.HasExited).ToArray();
            }
        }
        catch { }

        try
        {
            if (Directory.Exists(TargetPath))
            {
                Directory.Delete(TargetPath, true);
            }
            Directory.CreateDirectory(TargetPath);
            string log = Path.Combine(TargetPath, "stata.log");
            using (File.Create(log)) ;
        }
        catch (Exception ex)
        {
            throw new Exception(string.Format("Error at directory creation ({0})", TargetPath), ex);
        }

        _process = new Process();
        _process.StartInfo.WorkingDirectory = TargetPath;
        _process.StartInfo.UseShellExecute = false;
        _process.StartInfo.CreateNoWindow = true;
        _process.StartInfo.RedirectStandardOutput = true;
        _process.StartInfo.FileName = STATA_EXE;
        if (!string.IsNullOrEmpty(CommandLine))
        {
            _process.StartInfo.Arguments = _fullCommandLine;
        }
        else
        {
            throw new Exception("Error building command line");
        }
    }

    public void RunCommand(Action<string, string> callback)
    {
        try
        {
            if (!File.Exists(STATA_EXE))
            {
                throw new Exception(string.Format("Cannot find executable configured with StataExe parameter: [{0}]", STATA_EXE));
            }

            RenewProcess();
            int retry = 0;

            while (retry < RETRY)
            {
                while (!_process.Start()) { System.Threading.Thread.Sleep(200); }
                while (!_process.HasExited)
                {
                    if (DateTime.Now - _process.StartTime > TimeSpan.FromSeconds(TIME_OUT)
                        && new DirectoryInfo(TargetPath).GetFiles().Length == 0)
                    {
                        RenewProcess();
                        retry++;
                        break;
                    }
                }
                //System.Threading.Thread.Sleep(500);
                if (!File.Exists(Path.Combine(TargetPath, ConfigurationManager.AppSettings["TaxbenCsvFile"])))
                {
                    RenewProcess();
                    retry++;
                }
                else break;
            }
            _process.Dispose();
            callback(_folderURL, CommandLine);
        }
        catch (Exception ex)
        {
            try
            {
                throw new Exception(string.Format("Error executing Stata process ({0}, {1})", _process.ProcessName, _process.Id), ex);
            }
            catch
            {
                throw ex;
            }
        }
    }

    private void DeleteTempFolders(string path)
    {
        try
        {

            DirectoryInfo di = new DirectoryInfo(path).Parent;
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                if (DateTime.Now - dir.CreationTime > TimeSpan.FromHours(DELETE_AFTER)) dir.Delete(true);
            }
        }
        catch { }
    }

    private string _cmd;
    public string CommandLine
    {
        get
        {
            if (_cmd == null)
            {
                _cmd = GetTaxbenCommand();
            }
            return _cmd;
        }
    }


    private string _fullCommandLine
    {
        get
        {
            string cmd = ConfigurationManager.AppSettings["StataOptions"] ?? string.Empty;
            return string.Concat(cmd, " ", CommandLine);
        }
    }

    internal string GetTaxbenCommand()
    {
        string result;
        try
        {
            string template = ConfigurationManager.AppSettings["TaxbenCommand"];

            string commandByTempl = ConfigurationManager.AppSettings["TaxbenCommandBy"],
                output = null,
                commandBy = null;
            switch (_options.Output)
            {
                case TaxbenOptions.OutputBy.Country:
                    output = "output1";
                    commandBy = string.Format(commandByTempl, "perc", 1, "earnings");
                    break;
                case TaxbenOptions.OutputBy.Year:
                    output = "output2";
                    commandBy = string.Format(commandByTempl, "perc", 1, "earnings");
                    break;
                case TaxbenOptions.OutputBy.WorkingTime:
                    output = "output3";
                    var by  = _options.Adult1OutputBy ?? "earnings";
                    var min = 0.01;
                    var max = by == "earnings" ? 3 : 1;
                    commandBy = string.Format(commandByTempl, "perc", string.Format(CultureInfo.InvariantCulture, "{0}(0.01){1}", min, max), by);
                    break;
                case TaxbenOptions.OutputBy.Unemployment:
                    output = "output4";
                    commandBy = string.Format(commandByTempl, "level", string.Format("{0}/{1}", _options.MonthsOutOfWork[0], _options.MonthsOutOfWork[1]), "time");
                    break;
                default:
                    break;
            }
            

            result = string.Format(CultureInfo.InvariantCulture, template,
                output,                                    // 0
                string.Join(" ", _options.Years.Select(y => y.ToString())),
                string.Join(" ", _options.Countries),
                _options.Couple ? "couple" : "single",
                _options.AdultsAge,                                             // 4
                _options.Children,
                _options.Children > 0 ? _options.ChildrenAges[0] : 4,
                _options.Children > 1 ? _options.ChildrenAges[1] : 6,
                _options.Children > 2 ? _options.ChildrenAges[2] : 8,           // 8
                _options.Children > 3 ? _options.ChildrenAges[3] : 10,
                _options.Adult1Employed ? "empl" : "unempl",
                
                _options.Adult1Employed && _options.Adult1OutputBy != "earnings" // 11, first adult wage
                        ? GetWageCommand(true,  _options.Adult1Wage, _options.Adult1Earnings, _options.Adult1Amount)
                        : null, 

                _options.Adult1Employed ? (_options.Adult1WorkingTime == null ? 5 : Math.Round((double)_options.Adult1WorkingTime / 20, 2)) : 0,  // 12
                _options.SocialContribution == null ? "" : string.Format(ConfigurationManager.AppSettings["TaxbenCommandCRecord"], _options.SocialContribution),
                
                _options.Adult2Employed ?? false                                // 14, second adult wage
                        ? GetWageCommand(false,  _options.Adult2Wage, _options.Adult2Earnings, _options.Adult2Amount)
                        : null,
                
                _options.Adult2WorkingTime == null ? "0" : Math.Round((double)_options.Adult2WorkingTime / 20, 2).ToString(CultureInfo.InvariantCulture),
                _options.UnemploymentBenefit == true ? 1 : 0,                   // 16
                _options.SocialAssistance ? 1 : 0,                              // 17         
                _options.HousingBenefit ? 1 : 0,                                // 18
                _options.HousingBenefit                                         // 19 hosing costs
                    ? _options.HousingBenefitType == "Amount" 
                        ? string.Format("ncu_hc({0})", _options.HousingAmount ?? 0) 
                        : string.Format("hc({0})", Math.Round((double)(_options.AnnualHousingCosts ?? 20) / 100, 2).ToString(CultureInfo.InvariantCulture))
                    : "",

                _options.NewJobBenefit == true ? 1 : 0,                         // 20
                _options.InWorkBenefit == true ? 1 : 0,                         // 21
                _options.FamilyBenefit == true ? 1 : 0,                         // 22
                _options.ChildcareBenefit == true ? 1 : 0,                      // 23
                _options.HomecareBenefit == true ? 1 : 0,                       // 24
                _options.AloneParentBenefit == true ? 1 : 0,                    // 25

                _options.AmountCountry != null                                  // 26 
                    ? string.Format("ncu_country({0})", _options.AmountCountry)
                    : null,

                _options.MonthsOutOfWork!=null && _options.MonthsOutOfWork.Count == 1  // 27 - time
                    ? string.Format("time({0})", _options.MonthsOutOfWork[0])
                    : null,

                _options.NewJobBenefit == true && _options.CurrentJob.HasValue // 28 - time_iw
                    ? string.Format("time_iw({0})", _options.CurrentJob.Value)
                    : null,

                 _options.UnemploymentBenefit == true                          // 29 prWage_pr
                    ? GetPreviousWageCommand(_options.Adult1PreviousWage, _options.Adult1PreviousEarnings, _options.Adult1PreviousAmount)
                    : null,

                 commandBy,                                                     // 30
                ConfigurationManager.AppSettings["TaxbenSourceFolder"],         // 31
                TargetPath                                                      // 32
                );
        }
        catch (Exception ex)
        {
            throw new Exception("Error building command line", ex);
        }
        return result;
    }

    private string GetWageCommand(bool firstAdult, string wageOption, int? earnings, int? amount)
    {
        // "prWage_pr({0})"
        var template  = firstAdult
            ? "wage({0})" 
            : "wageSP({0})";

        var wage = "0";

        switch (wageOption)
        {
            case "Minimum":
                wage = "MIN";
                break;
            
            case "Amount":
                template = firstAdult 
                    ? "ncu_pr({0})" 
                    : "ncu_sp({0})";

                wage = amount.ToString();
                break;
            
            default:
                wage = (earnings ?? (firstAdult ? 100 : 0)) + "AW";
                break;
        }

        return string.Format(template, wage);
    }

    private string GetPreviousWageCommand(string wageOption, int? earnings, int? amount)
    {
        if (string.IsNullOrEmpty(wageOption) || wageOption == "Amount")
            return null;

        var template = @"prWage_pr({0})";
        string wage;

        switch (wageOption)
        {
            case "Minimum":
                wage = "MIN";
                break;

            default:
                wage = (earnings ?? 100) + "AW";
                break;
        }

        return string.Format(template, wage);
    }
}

/// <summary>
/// Summary description for TaxbenOptions
/// </summary>
public class TaxbenOptions
{
    public enum OutputBy
    {
        Country,
        Year,
        WorkingTime,
        Unemployment
    }
    public TaxbenOptions() { }

    public OutputBy Output { get; set; }

    public List<string> Countries { get; set; }

    public string CountryName { get; set; }

    public List<int> Years { get; set; }

    public bool Couple { get; set; }

    public int AdultsAge { get; set; }

    public int Children { get; set; }

    public List<int> ChildrenAges { get; set; }

    public bool Adult1Employed { get; set; }

    public int? Adult1Earnings{ get; set; }

    public int? Adult1WorkingTime { get; set; }

    public int? CurrentJob { get; set; }

    public int? SocialContribution { get; set; }

    public List<int> MonthsOutOfWork { get; set; }

    public bool? Adult2Employed { get; set; }

    public int? Adult2Earnings { get; set; }

    public int? Adult2WorkingTime { get; set; }

    public bool SocialAssistance { get; set; }

    public bool HousingBenefit { get; set; }

    

    public bool? UnemploymentBenefit { get; set; }

    public bool? NewJobBenefit { get; set; }

    public string Guid { get; set; }

    public string Adult1OutputBy { get; set; }
    public string Adult1Wage { get; set; }
    public int? Adult1Amount { get; set; }
    public string AmountCountry { get; set; }
    public string Adult2Wage { get; set; }
    public int? Adult2Amount { get; set; }
    public bool? InWorkBenefit { get; set; }
    public bool? FamilyBenefit { get; set; }
    public bool? ChildcareBenefit { get; set; }
    public bool? HomecareBenefit { get; set; }
    public bool? AloneParentBenefit { get; set; }

    public string Adult1PreviousWage { get; set; }
    public int? Adult1PreviousEarnings { get; set; }
    public int? Adult1PreviousAmount { get; set; }
    public string HousingBenefitType { get; set; }
    public int? AnnualHousingCosts { get; set; }
    public int? HousingAmount { get; set; }
}

public class CountryOutputProcessor : StataCsvOutputProcessor
{
    private List<KeyValuePair<string, double[]>> _bag = new List<KeyValuePair<string, double[]>>();

    public override void Process(Dictionary<string, int> map, string[] line, TaxbenOptions options, int index)
    {
        var country = line[map["country_name"]];
        var values  = this.Values(map, line, options, index);

        _bag.Add(new KeyValuePair<string, double[]>(country, values));
    }

    public override ITaxbenResult GetResult()
    {
        var ordered = _bag
                        .OrderBy(kv => kv.Value[0])
                        .ToArray();

        return new TaxbenResult<double[]>()
        {
            OrderedCategories = ordered.Select(kv => kv.Key).ToArray(),
            OrderedSeries = Transpose(ordered.Select(kv => kv.Value).ToArray())
        };
    }
}

public class YearOutputProcessor : StataCsvOutputProcessor
{
    private List<KeyValuePair<string, double[]>> _bag = new List<KeyValuePair<string, double[]>>();

    public override void Process(Dictionary<string, int> map, string[] line, TaxbenOptions options, int index)
    {
        var year = line[map["year"]];
        var values = this.Values(map, line, options, index);

        _bag.Add(new KeyValuePair<string, double[]>(year, values));
    }

    public override ITaxbenResult GetResult()
    {
        var ordered = _bag
            .OrderBy(kv => kv.Key)
            .ToArray();

        return new TaxbenResult<double[]>()
        {
            OrderedCategories = ordered.Select(kv => kv.Key).ToArray(),
            OrderedSeries = Transpose(ordered.Select(kv => kv.Value).ToArray())
        };
    }
}

public class WorkingTimeOutputProcessor : StataCsvOutputProcessor
{
    private Dictionary<string, List<KeyValuePair<string, double[]>>> _bag = new Dictionary<string, List<KeyValuePair<string, double[]>>>();

    public override void Process(Dictionary<string, int> map, string[] line, TaxbenOptions options, int index)
    {
        var country = line[map["country_name"]];
        var hoursWork = line[map[options.Adult1OutputBy == "earnings" ? "earnings" : "hours_work"]];
        var values = this.Values(map, line, options, index);
        List<KeyValuePair<string, double[]>> list;

        if (!_bag.TryGetValue(country, out list))
        {
            _bag[country] = list = new List<KeyValuePair<string, double[]>>();
        }

        list.Add(new KeyValuePair<string, double[]>(hoursWork, values));
    }

    public override ITaxbenResult GetResult()
    {
        return new TaxbenResult<TaxbenData<double[]>>()
        {
            OrderedCategories = _bag.Select(kv => kv.Key).ToArray(),
            OrderedSeries = _bag.Select(kv=>Expand(kv.Value)).ToArray()
        };
    }

    private TaxbenData<double[]> Expand(List<KeyValuePair<string, double[]>> list)
    {
        var ordered = list
            .OrderBy(kv => double.Parse(kv.Key, CultureInfo.InvariantCulture))
            .ToArray();

        return new TaxbenData<double[]>()
        {
            OrderedCategories = ordered.Select(kv => kv.Key).ToArray(),
            OrderedSeries = Transpose(ordered.Select(kv => kv.Value).ToArray())
        };
    }
}

public class UnemploymentOutputProcessor : StataCsvOutputProcessor
{
    private Dictionary<string, List<KeyValuePair<string, double[]>>> _bag = new Dictionary<string, List<KeyValuePair<string, double[]>>>();

    public override void Process(Dictionary<string, int> map, string[] line, TaxbenOptions options, int index)
    {
        var country = line[map["country_name"]];
        var timeNoJob = line[map["time_no_job"]];
        var values = this.Values(map, line, options, index);
        List<KeyValuePair<string, double[]>> list;

        if (!_bag.TryGetValue(country, out list))
        {
            _bag[country] = list = new List<KeyValuePair<string, double[]>>();
        }

        list.Add(new KeyValuePair<string, double[]>(timeNoJob, values));
    }

    public override ITaxbenResult GetResult()
    {
        return new TaxbenResult<TaxbenData<double[]>>()
        {
            OrderedCategories = _bag.Select(kv => kv.Key).ToArray(),
            OrderedSeries = _bag.Select(kv => Expand(kv.Value)).ToArray()
        };
    }

    private TaxbenData<double[]> Expand(List<KeyValuePair<string, double[]>> list)
    {
        var ordered = list
            .OrderBy(kv => int.Parse(kv.Key))
            .ToArray();

        return new TaxbenData<double[]>()
        {
            OrderedCategories = ordered.Select(kv => kv.Key).ToArray(),
            OrderedSeries = Transpose(ordered.Select(kv => kv.Value).ToArray())
        };
    }
}

public abstract class StataCsvOutputProcessor
{
    private static char SEP = ConfigurationManager.AppSettings["CSVSeparator"][0];
    private static string TaxbenCSVFile = ConfigurationManager.AppSettings["TaxbenCSVFile"];
    private static string TaxbenXLSFile = ConfigurationManager.AppSettings["TaxbenXLSFile"];
    private static string TaxbenLogFile = ConfigurationManager.AppSettings["TaxbenLogFile"];

    public static ITaxbenResult GetOuputData(
        TaxbenOptions options,
        string guid,
        string command,
        string folderUrl,
        string folderPath,
        string csvFilename = null
    )
    {
        if (string.IsNullOrEmpty(csvFilename))
        {
            csvFilename = Path.Combine(folderPath, TaxbenCSVFile);
        }

        var result = File.Exists(csvFilename)
                            ? ProcessCsv(csvFilename, GetProcessor(options.Output), options)
                            : new TaxbenResult<double[]>();

        result.Subtitle = GetSubtitle(options);
        result.Title = string.Format(GetTitle(options), options.CountryName, options.Years[0]);
        result.Guid = guid;
        result.TaxbenCommand = command;

        if (File.Exists(Path.Combine(folderPath, TaxbenXLSFile)))
        {
            result.ExcelURL = string.Concat(folderUrl, "/", TaxbenXLSFile);
        }

        if (File.Exists(Path.Combine(folderPath, TaxbenLogFile)))
        {
            result.LogURL = string.Concat(folderUrl, "/", TaxbenLogFile);
        }

        return result;
    }

    private static ITaxbenResult ProcessCsv(string filename, StataCsvOutputProcessor processor, TaxbenOptions options)
    {
        Dictionary<string, int> map = new Dictionary<string, int>();
        var index       = 0;
        string line;

        using (var sr = new System.IO.StreamReader(new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
        {
            while ((line = sr.ReadLine()) != null)
            {
                if (++index == 1)
                {
                    foreach (var kv in line.Split(SEP).Select((s, i) => new KeyValuePair<string, int>(s, i)))
                    {
                        map.Add(kv.Key, kv.Value);
                    }
                }
                else
                {
                    processor.Process(map, line.Split(SEP),options, index);
                }
            }
        }

        return processor.GetResult();
    }

    private static StataCsvOutputProcessor GetProcessor(TaxbenOptions.OutputBy output)
    {
        switch (output)
        {
            case TaxbenOptions.OutputBy.Country: return new CountryOutputProcessor();
            case TaxbenOptions.OutputBy.Year: return new YearOutputProcessor();
            case TaxbenOptions.OutputBy.WorkingTime: return new WorkingTimeOutputProcessor();
            case TaxbenOptions.OutputBy.Unemployment: return new UnemploymentOutputProcessor();
        }

        throw new NotImplementedException();
    }

    private static string GetSubtitle(TaxbenOptions options)
    {
        string subtitle = options.Couple ? "Couple" : "Single adult";
        subtitle = string.Concat(subtitle, " with ");
        if (options.Children == 0)
        {
            subtitle = string.Concat(subtitle, "no children");
        }
        else if (options.Children == 1)
        {
            subtitle = string.Concat(subtitle, "one child");
        }
        else
        {
            subtitle = string.Concat(subtitle, options.Children, " children");
        }
        return subtitle;
    }

    private static string GetTitle(TaxbenOptions options)
    {
        switch (options.Output)
        {
            case TaxbenOptions.OutputBy.Country: return "Net income by country, {1}";
            case TaxbenOptions.OutputBy.Year: return "Net income by year, {0}";
            case TaxbenOptions.OutputBy.WorkingTime: return options.Adult1OutputBy == "earnings"
                ? "Net income by gross wage levels, {0}, {1}" 
                : "Net income by hours of work, {0}, {1}";
            case TaxbenOptions.OutputBy.Unemployment: return "Net income by time spent out of work, {0}, {1}";
        }

        throw new NotImplementedException();
    }

    protected double[] Values(Dictionary<string, int> map, string[] data, TaxbenOptions options, int index)
    {
        try
        {
            var list = new List<double>()
            {
                Percent(map, data, "net", "aw"),
                -Percent(map, data, "it", "aw"),
                -Percent(map, data, "sc", "aw"),
                Percent(map, data, "iw", "aw"),
                Percent(map, data, "fb", "aw"),
                Percent(map, data, "hb", "aw"),
                Percent(map, data, "ub", "aw"),
                Percent(map, data, "sa", "aw"),
                Percent(map, data, "gross", "aw")
            };

            if (options.ChildcareBenefit == true)
            {
                list.Add(-Percent(map, data, "cc_cost", "aw"));
                list.Add(Percent(map, data, "cc_benefit", "aw"));
            }

            return list.ToArray();
        }
        catch (Exception ex)
        {
            throw new Exception(string.Format("A calculation could not be made because of an erroneous value in CSV file at line {0} at {1}: {2}", string.Join(",", data), index, ex.Message));
        }
    }

    protected double[][] Transpose(double[][] arr)
    {
        var rowCount = arr.Length;
        var columnCount = arr[0].Length;
        var transposed = new double[columnCount][];

        for (var column = 0; column < columnCount; column++)
        {
            transposed[column] = new double[rowCount];

            for (var row = 0; row < rowCount; row++)
            {
                transposed[column][row] = arr[row][column];
            }
        }

        return transposed;
    }

    private double Percent(Dictionary<string, int> map, string[] data, string column, string total)
    {
        return Math.Round(double.Parse(data[map[column]], CultureInfo.InvariantCulture) / double.Parse(data[map[total]], CultureInfo.InvariantCulture) * 100, 2);
    }

    public abstract void Process(Dictionary<string, int> map, string[] data, TaxbenOptions options, int index);
    public abstract ITaxbenResult GetResult();


}

public interface ITaxbenResult
{
    string Guid { get; set; }
    string ExcelURL { get; set; }
    string LogURL { get; set; }
    string Title { get; set; }
    string Subtitle { get; set; }
    string TaxbenCommand { get; set; }
    string ExceptionMessage { get; set; }
}

public class TaxbenResult<T> : TaxbenData<T>, ITaxbenResult
{
    public string Guid { get; set; }
    public string ExcelURL { get; set; }
    public string LogURL { get; set; }
    public string Title { get; set; }
    public string Subtitle { get; set; }
    public string TaxbenCommand { get; set; }
    public string ExceptionMessage { get; set; }
}

public class TaxbenData<T>
{
    public string[] OrderedCategories;
    public T[] OrderedSeries;
}