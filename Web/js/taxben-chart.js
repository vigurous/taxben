﻿var Logo = "https://upload.wikimedia.org/wikipedia/en/0/0d/OECD_logo_new.svg";
var Countries = {
    'Australia': 'AUS',
    'Austria': 'AUT',
    'Belgium': 'BEL',
    'Bulgaria': 'BGR',
    'Canada': 'CAN',
    'Chile': 'CHL',
    'Cyprus': 'CYP',
    'Czech Republic': 'CZE',
    'Denmark': 'DNK',
    'Estonia': 'EST',
    'Finland': 'FIN',
    'France': 'FRA',
    'Greece': 'GRC',
    'Germany': 'DEU',
    'Croatia': 'HRV',
    'Hungary': 'HUN',
    'Iceland': 'ISL',
    'Israel': 'ISR',
    'Ireland': 'IRL',
    'Italy': 'ITA',
    'Japan': 'JPN',
    'Lithuania': 'LTU',
    'Latvia': 'LVA',
    'Luxembourg': 'LUX',
    'Malta': 'MLT',
    'Netherlands': 'NLD',
    'Norway': 'NOR',
    'New Zealand': 'NZL',
    'Poland': 'POL',
    'Portugal': 'PRT',
    'Korea': 'KOR',
    'Romania': 'ROU',
    'Slovenia': 'SVN',
    'Slovak Republic': 'SVK',
    'Spain': 'ESP',
    'Sweden': 'SWE',
    'Switzerland': 'CHE',
    'Turkey': 'TUR',
    'United Kingdom': 'GBR',
    'United States': 'USA',
    'Russia':'RUS'
};

var Handler = "TaxbenHandlerNew.ashx";

var SeriesColors = {
    "Gross in-work earnings": "#AEBCC6",
    "Unemployment benefits": "#00B0F0",
    "Family benefits": "#E73741",
    "Social assistance": "#E1B400",
    "Housing benefits": "#6DB50D",
    "In-work benefits": "#38C2B2",
    "Social security contributions": "#BF41A4",
    "Income taxes": "#0C41AC",
    "Childcare benefits": "#74629d",
    "Childcare costs": "#cadd22"
};
var RetryCounter, RetryTimes = 3, WaitBeforeRetry = 1000;
var WaitTimeout = {
    "Country": 15,
    "Year": 15,
    "WorkingTime": 15,
    "Unemployment": 15
};

var Options, Exception;

function ajaxCall(options, cb) {
    var form = new FormData();
    form.append("options", JSON.stringify(options));

    $.ajax({
        type: "POST",
        url: Handler,
        data: form,
        processData: false,
        contentType: false,
        success: function (data) {
            if ($.isFunction(cb)) cb(data);
        },
        error: function () {
            wait(false);
            RetryCounter = RetryTimes;
            OnError({ ExceptionMessage: "Ajax error" });
        }
    });
}

function makeChart(options) {
    Options = options;

    $("#result-info-buttons").html($("#info-buttons").html());
    $("#taxben-result-footnote span").html(Config.version);

    $(".taxben-form").hide();
    $(".taxben-result").show();
    $("#testcommandline").hide();

    var chartContainer = $("#taxben-chart");

    wait(chartContainer);

    $(".taxben-result-button-container").hide();
    ajaxCall(options, function (data) {
        Exception = data.ExceptionMessage;
        if (!data) {
            OnError(data);
        } else if (!data.OrderedCategories || !data.OrderedCategories.length) {
            OnEmpty(data);
        } else {
            wait(false);
            setTimeout(function () {
                $("#taxben-chart .error").remove();
                $("#taxben-chart .no-data").remove();
            }, 100);

            switch (options.Output) {
                case "Country":
                    byCountryChart(chartContainer, data);
                    break;
                case "Year":
                    byYearChart(chartContainer, data);
                    break;
                case "WorkingTime":
                    byWorkingTimeChart(chartContainer, data);
                    break;
                case "Unemployment":
                    byUnemploymentChart(chartContainer, data);
                    break;
                default:
            }

            $(".taxben-result-button-container").show();
            $(".taxben-result-button-container #download-button")
                .toggle(data.ExcelURL!=null)
                .prop("href", data.ExcelURL)
                .prop("download", data.Title);

            $(".log-link")
                .attr("href", data.LogURL)
                .toggle(data.LogURL !== null);

            $("#testcommandline").show().text(data.TaxbenCommand);
        }
    });

    $(".taxben-result #return-button").on("click", function () {
        $(".taxben-form").show();
        $(".taxben-result").hide();
        $("#testcommandline").hide();
    });
}

function wait(cont) {
    $(".wait").remove();
    if (cont !== false) {
        var start = Date.now();
        var counter = WaitTimeout[Options.Output];
        cont.html("<div class='wait'>Please wait... <span>" + counter + "</span></div>");
        var fadeOut = function () {
            $(".wait span").html(--counter);
            $(".wait").animate({
                opacity: .2
            },{
                duration: 1000,
                complete: fadeIn
            });
        }, fadeIn = function () {
            if (Date.now() - start > WaitTimeout[Options.Output] * 1000) {
                $(".wait").remove();
                RetryCounter = RetryTimes;
                OnError({ ExceptionMessage: Exception || "No response, timeout" });
            } else {
                $(".wait span").html(--counter);
                $(".wait").animate({
                    opacity: 1
                },{
                    duration: 1000,
                    complete: fadeOut
                });
            }
        };
        fadeOut();
    }
}

function OnError(data) {
    Options.Guid = data.Guid;
    $("#taxben-chart .error").remove();

    if (RetryCounter > RetryTimes - 1) {
        wait(false);
        $("#taxben-chart").append($("<div class='error'>")
        .append($("<span title='" + (data ? Exception : "Response is empty") + "'>Sorry, an error occurred. </span>"))
        .append($("<br/>"))
        .append($("<a href='#' class='smaller'>Retry</a>").on("click", function () { makeChart(Options); }))
        .append($("<br/>"))
        .append($("<a href='#' target='_blank' download='log_session.log' class='smaller log-link'>View log</a>")));
        $(".taxben-result-button-container").show();
        $(".taxben-result-button-container #download-button").hide();
        if (data && data.LogURL) $(".log-link").attr("href", data.LogURL);
        else $(".log-link").hide();
        if (data && data.TaxbenCommand) $("#testcommandline").show().text(data.TaxbenCommand);
    }
    else {
        RetryCounter++;
        setTimeout(function () {
            makeChart(Options);         
        }, WaitBeforeRetry);
    }
}

function OnEmpty(data) {
    wait(false);

    $("#taxben-chart").append($("<div class='no-data'>")
        .append($("<span>No data to display</span>")));

    $(".taxben-result-button-container").show();
    $(".taxben-result-button-container #download-button").hide();

    $(".log-link")
        .attr("href", data.LogURL)
        .toggle(data.LogURL !== null);

    $("#testcommandline").show().text(data.TaxbenCommand);
}

function byCountryChart(container, data) {
    console.log(Options);
    container.highcharts({
        chart: {
            //animation: false,
            type: 'column',
            margin: [120, 10, 90, 50],
            events: {
                load: function () {
                    for (var i in this.series) {
                        if (this.series[i].type === 'column')
                            this.series[i].update({ color: SeriesColors[this.series[i].name] }, false);
                    }
                    this.redraw();

                    this.renderer.label('©', 665, 501)
                        .css({
                            color: '#575757',
                            fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                            fontWeight: 'normal',
                            fontSize: '10px'
                        }).add();
                    this.renderer.image(Logo, 680, 502, 60, 15).add();

                }
            }
        },
        credits: { enabled: false },
        exporting: {
            filename: 'Net household income by country',
            chartOptions: {
                chart: {
                    spacingTop: 30,
                    marginTop: 110,
                    marginBottom: 80,
                    events: {
                        load: function () {
                            this.renderer.label('©', 515, 380)
                                .css({
                                    color: '#575757',
                                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                                    fontWeight: 'normal',
                                    fontSize: '10px'
                                }).add();
                            this.renderer.image(Logo, 680, 502, 60, 15).add();
                        }
                    }
                },
                xAxis: {
                    lineWidth: 0,
                    tickWidth: 0
                },
                legend: {
                    floating: true,
                    y: 30,
                    x: 70,
                    height: 80,
                    itemWidth: 150,
                    itemDistance: 0,
                    itemMarginBottom: 1,
                    itemStyle: {
                        fontSize: '10px'
                    }
                }
            }
        },
        title: {
            text: data.Title,
            align: 'left',
            style: {
                color: '#0297C9',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontWeight: 'bold'
            }
        },
        subtitle: {
            text: data.Subtitle,
            align: 'left',
            y: 30,
            style: {
                color: '#7A7A7A',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontWeight: 'bold'
            }
        },
        xAxis: {
            lineWidth: 0,
            tickWidth: 0,
            categories: data.OrderedCategories,
            labels: {
                step: 1,
                rotation: data.OrderedCategories.length > 20 ? -45 : 0,
                style: {
                    color: '#575757',
                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                    fontSize: 12
                }
            }
        },
        yAxis: {
            plotLines: [{
                value: 0,
                color: '#0B1E2D',
                width: 1,
                zIndex: 5
            }],
            lineColor: '#0B1E2D',
            lineWidth: 1,
            tickWidth: 1,
            tickLength: 5,
            tickColor: '#0B1E2D',
            gridLineColor: '#CCCCCC',
            gridLineWidth: .5,
            title: {
                x: 6,
                text: '% of Average Wage',
                style: {
                    color: '#575757',
                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                    fontSize: 12
                }
            },
            labels: {
                x: -8,
                style: {
                    color: '#575757',
                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                    fontSize: 12
                }
            }
        },
        legend: {
            reversed: true,
            floating: false,
            x: 10,
            y: 40,
            verticalAlign: 'top',
            itemWidth: 175,
            itemDistance: 0,
            itemMarginBottom: 5,
            symbolRadius: 0,
            symbolHeight: 10,
            itemStyle: {
                color: '#575757',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontWeight: 'normal',
                fontSize: 12
            }
        },
        tooltip: {
            useHTML: true,
            borderColor: '#BAD3E2',
            backgroundColor: '#fff',
            headerFormat: '',
            pointFormatter: function () {
                var series = this.series.chart.series;
                var htm = '<b style="font-size:14px">' + this.category + '</b>&nbsp;&nbsp;<img width="20" ;" src="css/img/flags/' + Countries[this.category] + '.png"><br/>';
                htm += '<table style="margin-top:4px;">';
                for (var i = series.length - 1; i > 0; i--) {
                    if (series[i].visible) {
                        htm += '<tr><td>' + series[i].name + '</td><td style="padding-left:15px">' + series[i].data[this.x].y + '%</td></tr>';
                    }
                }
                htm += '<tr><td style="padding-top:4px;"><b>' + series[0].name + '</b></td><td style="padding-top:4px;padding-left:15px"><b>' + series[0].data[this.x].y + '%</b></td></tr>';
                htm += '</table>';
                return htm;
            },
            style: {
                color: '#575757',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontWeight: 'normal',
                fontSize: 10
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                borderWidth: 0
            },
            scatter: {
                states: {
                    hover: {
                        enabled: false
                    }
                },
                marker: {
                    symbol: 'diamond',
                    radius: 5,
                    fillColor: '#000'
                }
            }
        },
        series: [{
            name: 'Net household income',
            type: 'scatter',
            zIndex: 10,
            data: data.OrderedSeries[0]
        }, {
            name: 'Childcare costs',
            data: data.OrderedSeries[9],
            visible: Options.ChildcareBenefit || false,
            showInLegend: Options.ChildcareBenefit || false
        }, {
            name: 'Income taxes',
            data: data.OrderedSeries[1]
        }, {
            name: 'Social security contributions',
            data: data.OrderedSeries[2]
        }, {
            name: 'Childcare benefits',
            data: data.OrderedSeries[10],
            visible: Options.ChildcareBenefit || false,
            showInLegend: Options.ChildcareBenefit || false
        }, {
            name: 'In-work benefits',
            data: data.OrderedSeries[3]
        }, {
            name: 'Housing benefits',
            data: data.OrderedSeries[5]
        }, {
            name: 'Social assistance',
            data: data.OrderedSeries[7]
        }, {
            name: 'Family benefits',
            data: data.OrderedSeries[4]
        }, {
            name: 'Unemployment benefits',
            data: data.OrderedSeries[6]
        }, {
            name: 'Gross in-work earnings',
            data: data.OrderedSeries[8]
        }]
    });
}

function byYearChart(container, data) {
    container.highcharts({
        chart: {
            //animation: false,
            type: 'column',
            margin: [120, 10, 50, 50],
            events: {
                load: function () {
                    for (var i in this.series) {
                        if (this.series[i].type === 'column')
                            this.series[i].update({ color: SeriesColors[this.series[i].name] }, false);
                    }
                    this.redraw();

                    this.renderer.label('©', 665, 501)
                        .css({
                            color: '#575757',
                            fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                            fontWeight: 'normal',
                            fontSize: '10px'
                        }).add();
                    this.renderer.image(Logo, 680, 502, 60, 15).add();
                }
            }
        },
        credits: { enabled: false },
        exporting: {
            filename: 'Net income by year',
            chartOptions: {
                chart: {
                    spacingTop: 30,
                    marginTop: 110,
                    marginBottom: 40,
                    events: {
                        load: function () {
                            this.renderer.label('©', 515, 380)
                                .css({
                                    color: '#575757',
                                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                                    fontWeight: 'normal',
                                    fontSize: '10px'
                                }).add();
                            this.renderer.image(Logo, 530, 381, 60, 15).add();
                        }
                    }
                },
                xAxis: {
                    lineWidth: 0,
                    tickWidth: 0
                },
                legend: {
                    floating: true,
                    y: 30,
                    x: 70,
                    height: 80,
                    itemWidth: 150,
                    itemDistance: 0,
                    itemMarginBottom: 1,
                    itemStyle: {
                        fontSize: '10px'
                    }
                }
            }
        },
        title: {
            text: data.Title + '<span class="chart-country-flag"><img src="css/img/flags/' + Options.Countries[0] +  '.png"/></span>',
            useHTML:true,
            align: 'left',
            style: {
                color: '#0297C9',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontWeight: 'bold'
            }
        },
        subtitle: {
            text: data.Subtitle,
            align: 'left',
            y: 30,
            style: {
                color: '#7A7A7A',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontWeight: 'bold'
            }
        },
        xAxis: {
            lineWidth: 0,
            tickWidth: 0,
            categories: data.OrderedCategories,
            labels: {
                step: 1,
                style: {
                    color: '#575757',
                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                    fontSize: 12
                }
            }
        },
        yAxis: {
            plotLines: [{
                value: 0,
                color: '#0B1E2D',
                width: 1,
                zIndex: 5
            }],
            lineColor: '#0B1E2D',
            lineWidth: 1,
            tickWidth: 1,
            tickLength: 5,
            tickColor: '#0B1E2D',
            gridLineColor: '#CCCCCC',
            gridLineWidth: .5,
            title: {
                x: 6,
                text: '% of Average Wage',
                style: {
                    color: '#575757',
                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                    fontSize: 12
                }
            },
            labels: {
                x: -8,
                style: {
                    color: '#575757',
                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                    fontSize: 12
                }
            }
        },
        legend: {
            reversed: true,
            floating: false,
            x: 10,
            y: 40,
            verticalAlign: 'top',
            itemWidth: 175,
            itemDistance: 0,
            itemMarginBottom: 5,
            symbolRadius: 0,
            symbolHeight: 12,
            itemStyle: {
                color: '#575757',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontWeight: 'normal',
                fontSize: 12
            }
        },
        tooltip: {
            useHTML: true,
            borderColor: '#BAD3E2',
            backgroundColor: '#fff',
            headerFormat: '',
            pointFormatter: function () {
                var series = this.series.chart.series;
                var htm = '<b style="font-size:14px">' + this.category + '</b><br/>';
                htm += '<table style="margin-top:4px;">';
                for (var i = series.length - 1; i > 0; i--) {
                    if (series[i].visible) {
                        htm += '<tr><td>' + series[i].name + '</td><td style="padding-left:15px">' + series[i].data[this.x].y + '%</td></tr>';
                    }
                }
                htm += '<tr><td style="padding-top:4px;"><b>' + series[0].name + '</b></td><td style="padding-top:4px;padding-left:15px"><b>' + series[0].data[this.x].y + '%</b></td></tr>';
                htm += '</table>';
                return htm;
            },
            style: {
                color: '#575757',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontWeight: 'normal',
                fontSize: 10
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                borderWidth: 0
            },
            scatter: {
                states: {
                    hover: {
                        enabled: false
                    }
                },
                marker: {
                    symbol: 'diamond',
                    radius: 8,
                    fillColor: '#000'
                }
            }
        },
        series: [
            {
                name: 'Net household income',
                type: 'scatter',
                zIndex: 10,
                data: data.OrderedSeries[0]
            },
            {
                name: 'Childcare costs',
                data: data.OrderedSeries[9],
                visible: Options.ChildcareBenefit || false,
                showInLegend: Options.ChildcareBenefit || false
            },
            {
                name: 'Income taxes',
                data: data.OrderedSeries[1]
            },
            {
                name: 'Social security contributions',
                data: data.OrderedSeries[2]
            },
            {
                name: 'Childcare benefits',
                data: data.OrderedSeries[10],
                visible: Options.ChildcareBenefit || false,
                showInLegend: Options.ChildcareBenefit || false
            },
            {
                name: 'In-work benefits',
                data: data.OrderedSeries[3]
            },
            {
                name: 'Housing benefits',
                data: data.OrderedSeries[5]
            },
            {
                name: 'Social assistance',
                data: data.OrderedSeries[7]
            },
            {
                name: 'Family benefits',
                data: data.OrderedSeries[4]
            },
            {
                name: 'Unemployment benefits',
                data: data.OrderedSeries[6]
            },
            {
                name: 'Gross in-work earnings',
                data: data.OrderedSeries[8]
            }
        ]
    });
}

function byWorkingTimeChart(container, data) {
    $(".taxben-result-button-container-top").empty();
    for (var i = 0; i < data.OrderedCategories.length; i++) {
        $(".taxben-result-button-container-top").append($("<input type='button' id='multi-"+i+"' class='"+(i===0 ? 'multi active' : 'multi')+"' value='"+data.OrderedCategories[i]+"'>"));
    }
    $(".taxben-result-button-container-top").show();

    var coef = Options.Adult1OutputBy == "earnings" ? 1 : 100;
    var cats = [], v;
    for (var i = 0; i < data.OrderedSeries[0].OrderedCategories.length; i++) {
        v = Math.floor(parseFloat(data.OrderedSeries[0].OrderedCategories[i]) * coef);
        cats.push(v.toString());
    }

   
    var taxNeg = [], taxPos = [];
    for (i = 0; i < data.OrderedSeries[0].OrderedSeries[1].length; i++) {
        v = data.OrderedSeries[0].OrderedSeries[1][i];
        if (v === null) {
            taxNeg.push(null);
            taxPos.push(null);
        }
        else if (v <= 0) {
            taxNeg.push(v);
            taxPos.push(null);
        }
        else {
            taxNeg.push(null);
            taxPos.push(v);
        }
    }

    var toolTipHeader = Options.Adult1OutputBy == "earnings" ? 'Gross wage of the first adult %s% of the average wage' : 'Hours of work %s% of full-time work';
    var cartTitleArr = data.Title.split(',');

    if (cartTitleArr.length === 3) {
        cartTitle = cartTitleArr[0] + ', ' + data.OrderedCategories[0] + ',' + cartTitleArr[2];
    } else {
        cartTitle = data.Title;
    }

    cartTitle += '<span class="chart-country-flag"><img src="css/img/flags/' + Countries[data.OrderedCategories[0]] + '.png"/></span>';

    container.highcharts({
        chart: {
            //animation: false,
            type: 'area',
            margin: [120, 10, 50, 50],
            events: {
                load: function () {
                    for (var i in this.series) {
                        if (this.series[i].type === 'area')
                            this.series[i].update({ color: SeriesColors[this.series[i].name] }, false);
                    }
                    this.redraw();

                    this.renderer.label('©', 665, 501)
                        .css({
                            color: '#575757',
                            fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                            fontWeight: 'normal',
                            fontSize: '10px'
                        }).add();
                    this.renderer.image(Logo, 680, 502, 60, 15).add();
                }
            }
        },
        credits: { enabled: false },
        exporting: {
            filename: 'Net income by earnings levels',
            chartOptions: {
                chart: {
                    spacingTop: 30,
                    marginTop: 110,
                    marginBottom: 55,
                    events: {
                        load: function () {
                            this.renderer.label('©', 515, 380)
                                .css({
                                    color: '#575757',
                                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                                    fontWeight: 'normal',
                                    fontSize: '10px'
                                }).add();
                            this.renderer.image(Logo, 530, 381, 60, 15).add();
                        }
                    }
                },
                xAxis: {
                    lineWidth: 0,
                    tickWidth: 0
                },
                legend: {
                    floating: true,
                    y: 30,
                    x: 70,
                    height: 80,
                    itemWidth: 150,
                    itemDistance: 0,
                    itemMarginBottom: 1,
                    itemStyle: {
                        fontSize: '10px'
                    }
                }
            }
        },
        title: {
            text: cartTitle,
            useHTML: true,
            align: 'left',
            style: {
                color: '#0297C9',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontWeight: 'bold'
            }
        },
        subtitle: {
            text: data.Subtitle,
            align: 'left',
            y: 30,
            style: {
                color: '#7A7A7A',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontWeight: 'bold'
            }
        },
        xAxis: {
            startOnTick: true,
            endOnTick: true,
            lineWidth: 0,
            tickWidth: 0,
            title: {
                text: Options.Adult1OutputBy == "earnings" ?  'Gross wage of the first adult (% of the average wage)' : 'Hours of work (% of full-time work)',
                style: {
                    color: '#575757',
                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                    fontSize: 12
                }
            },
            categories: cats,
            labels: {
                step: Math.floor(cats.length / 10),                
                style: {
                    color: '#575757',
                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                    fontSize: 12
                }
            }
        },
        yAxis: {
            plotLines: [{
                value: 0,
                color: '#0B1E2D',
                width: 1,
                zIndex: 5
            }],
            lineColor: '#0B1E2D',
            lineWidth: 1,
            tickWidth: 1,
            tickLength: 5,
            tickColor: '#0B1E2D',
            gridLineColor: '#CCCCCC',
            gridLineWidth: .5,
            title: {
                x: 6,
                text: 'Net income (as % of average wage)',
                style: {
                    color: '#575757',
                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                    fontSize: 12
                }
            },
            labels: {
                x: -8,
                style: {
                    color: '#575757',
                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                    fontSize: 12
                }
            }
        },
        legend: {
            reversed: true,
            floating: false,
            x: 10,
            y: 40,
            verticalAlign: 'top',
            itemWidth: 175,
            itemDistance: 0,
            itemMarginBottom: 5,
            squareSymbol: false,
            symbolRadius: 0,
            symbolHeight: 12,
            symbolWidth: 20,
            itemStyle: {
                color: '#575757',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontWeight: 'normal',
                fontSize: 12
            }
        },
        tooltip: {
            useHTML: true,
            borderColor: '#BAD3E2',
            backgroundColor: '#fff',
            headerFormat: '',
            pointFormatter: function () {
                var series = this.series.chart.series;
                var htm = '<b style="font-size:14px">' + toolTipHeader.replace(/%s/, this.category) + '</b><br/>';
                htm += '<table style="margin-top:4px;">';
                for (var i = series.length - 2; i > 0; i--) {
                    if (!series[i].visible) {
                        continue;
                    }
                    else if (series[i].data[this.x].y === null) {
                        htm += '<tr><td>' + series[i].name + '</td><td style="padding-left:15px">' + series[9].data[this.x].y + '%</td></tr>';
                    } else {
                        htm += '<tr><td>' + series[i].name + '</td><td style="padding-left:15px">' + series[i].data[this.x].y + '%</td></tr>';
                    }
                }
                htm += '<tr><td style="padding-top:4px;"><b>' + series[0].name + '</b></td><td style="padding-top:4px;padding-left:15px"><b>' + series[0].data[this.x].y + '%</b></td></tr>';
                htm += '</table>';
                return htm;
            },
            style: {
                color: '#575757',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontWeight: 'normal',
                fontSize: 10
            }
        },
        plotOptions: {
            area: {
                //animation: false,
                lineWidth: 1,
                stacking: 'normal',
                states: {
                    hover: {
                        enabled: false
                    }
                },
                marker: {
                    enabled: false
                }
            },
            line: {
                //animation: false,
                zIndex: 10,
                lineWidth: 3,
                color: '#000',
                states: {
                    hover: {
                        enabled: false
                    }
                },
                marker: {
                    enabled: false
                }
            }
        },
        series: [{
            type: 'line',
            name: 'Net household income',
            data: data.OrderedSeries[0].OrderedSeries[0]
        },
        {
            stack: 1,
            name: 'Childcare costs',
            data: data.OrderedSeries[0].OrderedSeries[9],
            visible: Options.ChildcareBenefit || false,
            showInLegend: Options.ChildcareBenefit || false
        },
        {
            stack: 1,
            name: 'Income taxes',
            data: taxNeg
        },
        {
            stack: 1,
            name: 'Social security contributions',
            data: data.OrderedSeries[0].OrderedSeries[2]
        },
        {
            name: 'Childcare benefits',
            data: data.OrderedSeries[0].OrderedSeries[10],
            visible: Options.ChildcareBenefit || false,
            showInLegend: Options.ChildcareBenefit || false
        },
        {
            name: 'In-work benefits',
            data: data.OrderedSeries[0].OrderedSeries[3]
        }, {
            name: 'Housing benefits',
            data: data.OrderedSeries[0].OrderedSeries[5]
        }, {
            name: 'Social assistance',
            data: data.OrderedSeries[0].OrderedSeries[7]
        }, {
            name: 'Family benefits',
            data: data.OrderedSeries[0].OrderedSeries[4]
        }, {
            name: 'Unemployment benefits',
            data: data.OrderedSeries[0].OrderedSeries[6]
        }, {
            name: 'Gross in-work earnings',
            data: data.OrderedSeries[0].OrderedSeries[8]
        }, {
            showInLegend: false,
            name: 'Income taxes',
            data: taxPos
        }]
    });

    var mychart = Highcharts.charts.slice(-1)[0];

    
    data.OrderedCategories.forEach(function (countryName, countryIndex) {
        var btn = document.getElementById("multi-" + countryIndex);

        btn.addEventListener('click', function () {

            $('input.multi.active').removeClass('active');
            $('input#'+this.id).addClass('active');

            var taxNeg = [], taxPos = [];
            for (var i = 0; i < data.OrderedSeries[countryIndex].OrderedSeries[1].length; i++) {
                v = data.OrderedSeries[countryIndex].OrderedSeries[1][i];
                if (v === null) {
                    taxNeg.push(null);
                    taxPos.push(null);
                }
                else if (v <= 0) {
                    taxNeg.push(v);
                    taxPos.push(null);
                }
                else {
                    taxNeg.push(null);
                    taxPos.push(v);
                }
            }

            var cartTitleArr = data.Title.split(',');
            if (cartTitleArr.length === 3) {
                cartTitle = cartTitleArr[0] + ', ' + countryName + ',' + cartTitleArr[2];
            } else {
                cartTitle = data.Title;
            }

            cartTitle += '<span class="chart-country-flag"><img src="css/img/flags/' + Countries[countryName] + '.png"/></span>';

            mychart.update({
                title: {
                    text: cartTitle,
                    useHTML:true
                },
                series: [{
                    type: 'line',
                    name: 'Net household income',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[0]
                }, {
                    stack: 1,
                    name: 'Childcare costs',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[9],
                    visible: Options.ChildcareBenefit || false,
                    showInLegend: Options.ChildcareBenefit || false
                },{
                    stack: 1,
                    name: 'Income taxes',
                    data: taxNeg
                }, {
                    stack: 1,
                    name: 'Social security contributions',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[2]
                }, {
                    name: 'Childcare benefits',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[10],
                    visible: Options.ChildcareBenefit || false,
                    showInLegend: Options.ChildcareBenefit || false
                }, {
                    name: 'In-work benefits',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[3]
                }, {
                    name: 'Housing benefits',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[5]
                }, {
                    name: 'Social assistance',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[7]
                }, {
                    name: 'Family benefits',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[4]
                }, {
                    name: 'Unemployment benefits',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[6]
                }, {
                    name: 'Gross in-work earnings',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[8]
                }, {
                    showInLegend: false,
                    name: 'Income taxes',
                    data: taxPos
                }]
            }, true, false, {
                duration: 800
            });
        });
    });
}

function byUnemploymentChart(container, data) {

    $(".taxben-result-button-container-top").empty();
    for (var i = 0; i < data.OrderedCategories.length; i++) {
        $(".taxben-result-button-container-top").append($("<input type='button' id='multi-"+i+"' class='"+(i===0 ? 'multi active' : 'multi')+"' value='"+data.OrderedCategories[i]+"'>"));
    }
    $(".taxben-result-button-container-top").show();

    var cats = [], v, min = Infinity, max = -Infinity;
    for (var i = 0; i < data.OrderedSeries[0].OrderedCategories.length; i++) {
        v = parseInt(data.OrderedSeries[0].OrderedCategories[i]);
        cats.push(v.toString());
        min = Math.min(min, v);
        max = Math.max(max, v);
    }
    var int = 10;
    if (max - min < 10) int = 1;
    else if (max - min < 40) int = 2;
    else if (max - min < 60) int = 4;

    var taxNeg = [], taxPos = [];
    for (i = 0; i < data.OrderedSeries[0].OrderedSeries[1].length; i++) {
        v = data.OrderedSeries[0].OrderedSeries[1][i];
        if (v === null) {
            taxNeg.push(null);
            taxPos.push(null);
        }
        else if (v <= 0) {
            taxNeg.push(v);
            taxPos.push(null);
        }
        else {
            taxNeg.push(null);
            taxPos.push(v);
        }
    }

    var cartTitleArr = data.Title.split(',');
    if (cartTitleArr.length === 3) {
        cartTitle = cartTitleArr[0] + ', ' + data.OrderedCategories[0] + ',' + cartTitleArr[2];
    } else {
        cartTitle = data.Title;
    }

    cartTitle += '<span class="chart-country-flag"><img src="css/img/flags/' + Countries[data.OrderedCategories[0]] + '.png"/></span>';

    container.highcharts({
        chart: {
            //animation: false,
            type: 'area',
            margin: [120, 10, 50, 50],
            events: {
                load: function () {
                    for (var i in this.series) {
                        if (this.series[i].type === 'area')
                            this.series[i].update({ color: SeriesColors[this.series[i].name] }, false);
                    }
                    this.redraw();

                    this.renderer.label('©', 665, 501)
                        .css({
                            color: '#575757',
                            fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                            fontWeight: 'normal',
                            fontSize: '10px'
                        }).add();
                    this.renderer.image(Logo, 680, 502, 60, 15).add();
                }
            }
        },
        credits: { enabled: false },
        exporting: {
            filename: 'Net household income by unemployment duration',
            chartOptions: {
                chart: {
                    spacingTop: 30,
                    marginTop: 110,
                    marginBottom: 50,
                    events: {
                        load: function () {
                            this.renderer.label('©', 515, 380)
                                .css({
                                    color: '#575757',
                                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                                    fontWeight: 'normal',
                                    fontSize: '10px'
                                }).add();
                            this.renderer.image(Logo, 530, 381, 60, 15).add();
                        }
                    }
                },
                legend: {
                    floating: true,
                    y: 30,
                    x: 70,
                    height: 80,
                    itemWidth: 150,
                    itemDistance: 0,
                    itemMarginBottom: 1,
                    itemStyle: {
                        fontSize: '10px'
                    }
                }
            }
        },
        title: {
            text: cartTitle,
            useHTML:true,
            align: 'left',
            style: {
                color: '#0297C9',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontWeight: 'bold'
            }
        },
        subtitle: {
            text: data.Subtitle,
            align: 'left',
            y: 30,
            style: {
                color: '#7A7A7A',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontWeight: 'bold'
            }
        },
        xAxis: {
            startOnTick: true,
            endOnTick: true,
            lineWidth: 0,
            tickWidth: 0,
            categories: cats,
            title: {
                text: 'Unemployment duration (months)',
                style: {
                    color: '#575757',
                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                    fontSize: 12
                }
            },
            labels: {
                step: 1,
                formatter: function () {
                    if (parseInt(this.value) !== this.pos && parseInt(this.value) % int === 0) {
                        return this.value;
                    }
                    else return null;
                },
                style: {
                    color: '#575757',
                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                    fontSize: 12
                }
            }
        },
        yAxis: {
            plotLines: [{
                value: 0,
                color: '#0B1E2D',
                width: 1,
                zIndex: 5
            }],
            lineColor: '#0B1E2D',
            lineWidth: 1,
            tickWidth: 1,
            tickLength: 5,
            tickColor: '#0B1E2D',
            gridLineColor: '#CCCCCC',
            gridLineWidth: .5,
            title: {
                x: 6,
                text: 'Net income (% of average wage)',
                style: {
                    color: '#575757',
                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                    fontSize: 12
                }
            },
            labels: {
                x: -8,
                style: {
                    color: '#575757',
                    fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                    fontSize: 12
                }
            }
        },
        legend: {
            reversed: true,
            floating: false,
            x: 120,
            y: 40,
            verticalAlign: 'top',
            itemWidth: 190,
            itemDistance: 10,
            itemMarginBottom: 5,
            squareSymbol: false,
            symbolRadius: 0,
            symbolHeight: 12,
            symbolWidth: 20,
            itemStyle: {
                color: '#575757',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontWeight: 'normal',
                fontSize: 12
            }
        },
        tooltip: {
            useHTML: true,
            borderColor: '#BAD3E2',
            backgroundColor: '#fff',
            headerFormat: '',
            pointFormatter: function () {
                var series = this.series.chart.series;
                var htm = '<b style="font-size:14px">' + this.category + ' months without a job</b><br/>';
                htm += '<table style="margin-top:4px;">';
                for (var i = series.length - 2; i > 0; i--) {
                    if (series[i].data[this.x].y === null)
                        htm += '<tr><td>' + series[i].name + '</td><td style="padding-left:15px">' + series[9].data[this.x].y + '%</td></tr>';
                    else htm += '<tr><td>' + series[i].name + '</td><td style="padding-left:15px">' + series[i].data[this.x].y + '%</td></tr>';
                }
                htm += '<tr><td style="padding-top:4px;"><b>' + series[0].name + '</b></td><td style="padding-top:4px;padding-left:15px"><b>' + series[0].data[this.x].y + '%</b></td></tr>';
                htm += '</table>';
                return htm;
            },
            style: {
                color: '#575757',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontWeight: 'normal',
                fontSize: 10
            }
        },
        plotOptions: {
            area: {
                //animation: false,
                lineWidth: 1,
                stacking: 'normal',

                states: {
                    hover: {
                        enabled: false
                    }
                },
                marker: {
                    enabled: false
                }
            },
            line: {
                //animation: false,
                zIndex: 10,
                lineWidth: 3,
                color: '#000',
                states: {
                    hover: {
                        enabled: false
                    }
                },
                marker: {
                    enabled: false
                }
            }
        },
        series: [{
                type: 'line',
                name: 'Net household income',
                data: data.OrderedSeries[0].OrderedSeries[0]
            }, {
                stack: 1,
                name: 'Income taxes',
                data: taxNeg
            }, {
                stack: 1,
                name: 'Social security contributions',
                data: data.OrderedSeries[0].OrderedSeries[2]
            },     {
                name: 'In-work benefits',
                data: data.OrderedSeries[0].OrderedSeries[3]
            }, {
                name: 'Housing benefits',
                data: data.OrderedSeries[0].OrderedSeries[5]
            }, {
                name: 'Social assistance',
                data: data.OrderedSeries[0].OrderedSeries[7]
            }, {
                name: 'Family benefits',
                data: data.OrderedSeries[0].OrderedSeries[4]
            }, {
                name: 'Unemployment benefits',
                data: data.OrderedSeries[0].OrderedSeries[6]
            }, {
                name: 'Gross in-work earnings',
                data: data.OrderedSeries[0].OrderedSeries[8]
            }, {
                showInLegend: false,
                name: 'Income taxes',
                data: taxPos
            }
        ]
    });

    var mychart = Highcharts.charts.slice(-1)[0];

    data.OrderedCategories.forEach(function (countryName, countryIndex) {
        var btn = document.getElementById("multi-" + countryIndex);

        btn.addEventListener('click', function () {

            $('input.multi.active').removeClass('active');
            $('input#' + this.id).addClass('active');

            var taxNeg = [], taxPos = [];
            for (var i = 0; i < data.OrderedSeries[countryIndex].OrderedSeries[1].length; i++) {
                v = data.OrderedSeries[countryIndex].OrderedSeries[1][i];
                if (v === null) {
                    taxNeg.push(null);
                    taxPos.push(null);
                }
                else if (v <= 0) {
                    taxNeg.push(v);
                    taxPos.push(null);
                }
                else {
                    taxNeg.push(null);
                    taxPos.push(v);
                }
            }

            var cartTitleArr = data.Title.split(',');
            if (cartTitleArr.length === 3) {
                cartTitle = cartTitleArr[0] + ', ' + countryName + ',' + cartTitleArr[2];
            } else {
                cartTitle = data.Title;
            }
            cartTitle += '<span class="chart-country-flag"><img src="css/img/flags/' + Countries[countryName] + '.png"/></span>';

            mychart.update({
                title: {
                    text: cartTitle,
                    useHTML:true
                },
                series: [{
                    type: 'line',
                    name: 'Net household income',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[0]
                }, {
                    stack: 1,
                    name: 'Income taxes',
                    data: taxNeg
                }, {
                    stack: 1,
                    name: 'Social security contributions',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[2]
                },     {
                    name: 'In-work benefits',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[3]
                }, {
                    name: 'Housing benefits',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[5]
                }, {
                    name: 'Social assistance',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[7]
                }, {
                    name: 'Family benefits',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[4]
                }, {
                    name: 'Unemployment benefits',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[6]
                }, {
                    name: 'Gross in-work earnings',
                    data: data.OrderedSeries[countryIndex].OrderedSeries[8]
                }, {
                    showInLegend: false,
                    name: 'Income taxes',
                    data: taxPos
                }]
            }, true, false, {
                duration: 800
            });
        });
    });
}