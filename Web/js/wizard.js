﻿var OnWizardStepChange = null;

(function ($) {

    $.fn.wizard = function (options, value) {
        var self = this;

        if (options === "index") {

            var prev = self.find(".wizard-step.wizard-step-current").index();

            if (!isNaN(value)) {

                var advanceFunc = function() {
                    value = parseInt(value);
                    var settings = self.data("settings");

                    self.find(".wizard-path-step").each(function (index) {
                        $(this).toggleClass("active", index === value);
                    });

                    self.find(".wizard-steps-tray").animate({
                        marginLeft: -value * self.find(".wizard-steps-container").width()
                    }, 200, "swing", function () {
                        self.find(".wizard-step").removeClass("wizard-step-current").eq(value).addClass("wizard-step-current");
                    });
                    OnWizardStepChange = null;
                    settings.on.change(prev, value);
                }

                if (value>prev && OnWizardStepChange !== null)
                {
                    OnWizardStepChange(advanceFunc);
                }
                else
                {
                    advanceFunc();
                }
            }
            else return prev;
        }
        else
        {
            this.hide();

            // Default
            var settings = $.extend(true, {
                selectedIndex: 0,
                css: {
                    border: "none",
                    width: 800,
                    height: 600,
                    backgroundColor: "#fff"
                },
                on: {
                    load: $.noop,
                    change: $.noop,
                    reset: $.noop,
                    run: $.noop
                }
            }, options);


            var wizard = $("<div class='wizard-main " + this.attr("class") + "' id='"
                + this.attr("id") + "-rendered'>").data("settings", settings);
            if (settings.css) wizard.css(settings.css);
            wizard.insertAfter(self);
            var path = $("<div class='wizard-path-container'><table cellpadding='0' cellspacing='0' class='wizard-path-table'><tr></tr></table></div>");
            var steps = $("<div class='wizard-steps-container'>");
            var tray = $("<div class='wizard-steps-tray'>").appendTo(steps);
            var buttons = $("<div class='wizard-buttons-container'>");
            this.filter("ul").children("li").each(function (index) {
                var step = $(this);
                
                if (!step.hasClass("wizard-buttons-container"))
                {
                    //if (index > 0) {
                    //    path.append($("<span class='wizard-path-separator'>"));
                    var stepsnb = step.parent("ul").find("li[data-wizard-step]").length;
                    $("tr", path).append($("<td>", {
                        "class": "wizard-path-step" 
                           + (index === settings.selectedIndex ? " active" : "")
                           + (index === stepsnb - 1 ? " last" : ""),
                        text: step.attr("data-wizard-step") 
                            + (index < stepsnb - 1 ? "  ►" : ""),
                        on: {
                            click: function () {
                                wizard.wizard("index", index);
                            }
                        }
                    }));
                    tray.append($("<div>", {
                        "class": "wizard-step",
                        width: wizard.width()
                    }).append($("<div>", {
                        "class": "wizard-step-content",
                        html: step.html()
                    })));
                }
                else 
                {
                    buttons.html(step.html());
                    $("#wizard-prev-button", buttons).on("click", function () {
                        if ($(this).prop("disabled") || $(this).hasClass("disabled")) return false;
                        var index = wizard.wizard("index");
                        wizard.wizard("index", index - 1);
                    });
                    $("#wizard-next-button", buttons).on("click", function () {
                        if ($(this).prop("disabled") || $(this).hasClass("disabled")) return false;
                        var index = wizard.wizard("index");
                        wizard.wizard("index", index + 1);
                    });
                    $("#wizard-reset-button", buttons).on("click", function () {
                        if ($(this).prop("disabled") || $(this).hasClass("disabled")) return false;
                        settings.on.reset();
                    });
                    $("#wizard-run-button", buttons).on("click", function () {
                        if ($(this).prop("disabled") || $(this).hasClass("disabled")) return false;
                        settings.on.run();
                    });
                }
           });
            


            wizard.append(path).append(steps).append(buttons);
            self.remove();


            wizard.find(".wizard-path-step").each(function (index) {
                $(this).toggleClass("active", index === settings.selectedIndex);
            });
            wizard.find(".wizard-steps-tray").css({
                marginLeft: -settings.selectedIndex * wizard.find(".wizard-steps-container").width()
            });
            wizard.find(".wizard-step").removeClass("wizard-step-current")
                .eq(settings.selectedIndex).addClass("wizard-step-current");

            settings.on.load(wizard);

            return wizard;
        }
        return this;
    };

}(jQuery));