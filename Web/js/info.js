﻿(function ($) {

    $.fn.info = function (options) {
        var self = this;


        this.hide();

        // Default
        var settings = $.extend(true, {
            bulletHtml: "?",
            bulletClass: "info-bullet",
            useDiv: true,
            divClass: null,
            on: {
                load: $.noop,
                mouseOver: $.noop,
                mouseOut: $.noop
            }
        }, options);

        $.globalEval("var _timeout = [], _hover = [];");

        var result = $();
        this.each(function (i) {
            var id = $(this).attr("id");
            $(this).attr("id", id + "__");

            $('#' + id + "-content").remove();
            var info = $("<div class='info-main " + $(this).attr("class") + "' id='"
                + id + "-content'>").data("settings", settings)
                .hide().html($(this).html()).appendTo("body");//.insertAfter($(this));
            if (settings.css) info.css(settings.css);

            var bullet = $("<div>", {
                id: id,
                "class": settings.bulletClass,
                html: settings.bulletHtml,
                title: (settings.useDiv ? null : $.trim($(this).text()))
            }).insertAfter($(this));


            info.hover(function () {
                $(this).data("locked", true);
            }, function () {
                $(this).data("locked", false);
                _timeout[i] = setTimeout(function () {
                    $("#" + id + "-content").fadeOut(100, function () {
                            clearTimeout(_timeout[i]);
                        });
                }, 100);
            });

            bullet.hover(
                function () {
                    if (settings.useDiv) {
                        clearTimeout(_timeout[i]);
                        $("#" + id + "-content").css({
                            position: "absolute",
                            top: bullet.offset().top,
                            left: bullet.offset().left + bullet.width() + 10
                        }).fadeIn(200);
                    }
                    settings.on.mouseOver();
                },
                function () {
                    if (settings.useDiv) {
                        _timeout[i] = setTimeout(function () {
                            if (!$("#" + id + "-content").data("locked")) {
                                $("#" + id + "-content").fadeOut(100, function () {
                                    clearTimeout(_timeout[i]);
                                });
                            }
                        }, 500);
                    }
                    settings.on.mouseOut();
                });

            $(this).remove();
            bullet.prev().css({ display: "inline-block" });
            settings.on.load();
            result = result.add(bullet);
        });

        return result;
    };

}(jQuery));