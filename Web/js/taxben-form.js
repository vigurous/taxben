﻿var notOECD = ["BGR", "CYP", "HRV", "MLT", "ROU", "RUS"];
var notUE27 = ["GBR", "AUS", "CAN", "CHL", "ISL", "ISR", "JPN", "NOR", "NZL", "KOR", "CHE", "TUR", "USA", "RUS"];

var Wizard, Form = {}, Config = {}, CountryMap = {};
var Labels = {
    Output: function (val) {
        var text = Form.Output == "WorkingTime"
            ? Form.Adult1OutputBy == "earnings" ? "Output by gross wage levels" : "Output by hours of work"
            : $(".taxben-form .step-1 *[data-taxben-value=" + val + "]").parent().attr("title");
        return [1, "Output type", text];
    },
    Countries: function (val) {
        var elt1, elt2, elt3;
        var list = $.map(val, function (c) { return CountryMap[c]; }).join(", ");

        if (val.length > 1) {
            elt1 = "Country list";
            elt2 = val.length + " countries";
            $(".taxben-form .step-6 #info-countries-list").remove();
            elt3 = $("<div class='detail' id='info-countries-list'>" + list + "</div>");
        }
        else {
            elt1 = "Country";
            elt2 = list;
        }

        return [2, elt1, elt2, elt3];
    },
    Years: function (val) {
        var elt1, elt2, elt3;
        elt1 = (val.length > 1 ? "Year list" : "Year");
        if (val.length > 4) {
            elt2 = val.length + " years";
            $("#info-years-list").remove();
            elt3 = $("<div class='detail' id='info-years-list'>" + val.join(", ") + "</div>");
        }
        else elt2 = val.join(", ");

        return [2, elt1, elt2, elt3];
    },
    Couple: function (val) {
        return [3, "Family type", (val ? "Couple" : "Single person")];
    },
    AdultsAge: function (val) {
        return [3, "Age of adults", val + " years old"];
    },
    Children: function (val) {
        return [3, "Number of children", (val === 0 ? "No children" : val)];
    },
    ChildrenAges: function (val) {
        return [3, "Age of children", (val === null ? "n/a" : val.join(", ") + " year" + (val[val.length - 1] === 1 ? "" : "s") + " old")];
    },
    Adult1Employed: function (val) {
        return [4, "Activity status (first adult)", (val ? "Employed" : "Without a job")];
    },
    Adult1Earnings: function (val) {
        text = "n/a";
        if (!Form.Adult1Employed) { text = "n/a"; }
        else if (Form.Adult1Wage === "Percent") { text = Form.Adult1Earnings + "% of average wage"; }
        else if (Form.Adult1Wage === "Minimum") {text = "Minimum";}
        else if (Form.Adult1Wage === "Amount") { text = Form.Adult1Amount ? Form.Adult1Amount + " NCU, " + (Form.AmountCountry ? CountryMap[Form.AmountCountry] : "") : "n/a";}
        return [4, "Gross hourly wage (first adult)", text];
    },
    Adult1WorkingTime: function (val) {
        return [4, "Hours of work per week (first adult)", (isEmpty(val) ? "n/a" : val + "% of full-time")];
    },
    SocialContribution: function (val) {
        var text;
        if (val != null) {
            var y = Math.floor(val / 12);
            var m = val % 12;
            text = (y === 0 ? "" : (y + " year" + (y === 1 ? "" : "s"))) +
                (m === 0 ? "" : (y === 0 ? "" : " and ") + m + " month" + (m === 1 ? "" : "s"));
        }
        return [4, "Total number of months of social security contributions", (val === null ? "n/a" : (val === 0 ? "None" : text))];
    },
    MonthsOutOfWork: function (val) {
        var step = Form.Output === "Unemployment"
            ? -1
            : Form.Adult1Employed && Form.NewJobBenefit ? 5 : 4;

        return [step, "Time spent without a job (first adult)", (val === null ? "n/a" : (val.length === 1 ? val[0] : val[0] + " to " + val[1]) + " month" + ((val[1] || val[0]) > 1 ? "s" : ""))];
    },
    Adult1PreviousEarnings: function (val) {
        text = "n/a";
        if (Form.Adult1PreviousWage === "Percent") { text = Form.Adult1PreviousEarnings + "% of average wage"; }
        else if (Form.Adult1PreviousWage === "Minimum") { text = "Minimum"; }
        else if (Form.Adult1PreviousWage === "Amount") { text = Form.Adult1PreviousAmount ? Form.Adult1PreviousAmount + " NCU, " + (Form.AmountCountry ? CountryMap[Form.AmountCountry] : "") : "n/a"; }
        return [5, "Previous annual earnings (first adult)", text];
    },
    Adult2Employed: function (val) {
        return [4, "Activity status (second adult)", (val === null ? "n/a" : (val ? "Employed" : "Without a job"))];
    },
    Adult2Earnings: function (val) {
        var text = "n/a";
        if (!Form.Couple || ! Form.Adult2Employed) {text = "n/a";}
        else if (Form.Adult2Wage === "Percent") { text = Form.Adult2Earnings + "% of average wage"; }
        else if (Form.Adult2Wage === "Minimum") {text = "Minimum";}
        else if (Form.Adult2Wage === "Amount") { text = Form.Adult2Amount ? Form.Adult2Amount + " NCU, " + (Form.AmountCountry ? CountryMap[Form.AmountCountry] : "") : "n/a";}
        return [4, "Gross hourly wage (second adult)", text];
    },
    Adult2WorkingTime: function (val) {
        return [4, "Hours of work per week (second adult)", (val === null ? "n/a" : val + "% of full-time")];
    },
    UnemploymentBenefit: function (val) {
        return [5, "Unemployment benefits", val ? "Yes" : "No"];
    },
    SocialAssistance: function (val) {
        return [5, "Social assistance / GMI benefits", (val ? "Yes" : "No")];
    },
    HousingBenefit: function (val) {
        return [5, "Cash housing benefits", (val ? "Yes" : "No")];
    },
    AnnualHousingCosts: function (val) {
        var text = "n/a";
        if (Form.HousingBenefitType === "Percent") { text = Form.AnnualHousingCosts + "% of average wage"; }
        else if (Form.HousingBenefitType === "Amount") { text = Form.HousingAmount ? Form.HousingAmount + " NCU, " + (Form.AmountCountry ? CountryMap[Form.AmountCountry] : "") : "n/a"; }
        return [5, "Annual housing costs", text];
    },
    NewJobBenefit: function (val) {
        return [5, "Temporary into-work benefits when starting a new job (first adult)", (val === null ? "n/a" : (val ? "Yes" : "No"))];
    },
    CurrentJob: function (val) {
        return [5, "Months in the new job (first adult)", (val === null ? "n/a" : $("#months-to-years1").text().replace("(", "").replace(")", ""))];
    },
    InWorkBenefit: function (val) {
        return [5, "In-work benefits", val ? "Yes" : "No"];
    },
    FamilyBenefit: function (val) {
        return [5, "Family benefits", val ? "Yes" : "No"];
    },
    ChildcareBenefit: function (val) {
        return [5, "Benefits and costs related to the use of centre based childcare", (val === null ? "n/a" : (val ? "Yes" : "No"))];
    },
    HomecareBenefit: function (val) {
        return [5, "Homecare allowances", (val === null ? "n/a" : (val ? "Yes" : "No"))];
    },
    AloneParentBenefit: function (val) {
        return [5, "Lone parent maintenance benefits", (val === null ? "n/a" : (val ? "Yes" : "No"))];
    }
}

function isEmpty(val) { return (val === undefined || val == null || val.length <= 0) ? true : false;}

function initWizard(load, transition) {

    $.getJSON('js/config.json', function (data) {
        Config = data;
        Form.Countries = [];
    });

    $('.taxben-form .step-2 .country-list input[type=checkbox]').map(function(i, cb) {
        var $cb = $(cb);
        CountryMap[$cb.attr("data-taxben-value")] = $cb.attr('title');
    });

    $(".taxben-result").hide();

    Wizard = $("#taxben-wizard").wizard({
        selectedIndex: 0,
        useStepTextInStartButton: false,
        useStepTextsInButtons: false,
        css: {
            width: 800,
            height: 680
        },
        on: {
            load: initTaxbenForm,
            change: transitionTaxbenForm,
            reset: resetTaxbenForm,
            run: function () {
                Wizard.wizard("index", 6);
            }
        }
    });
}

function initTaxbenForm(wizard) {
    Wizard = wizard;

    $(".wizard-buttons-container input").hide();
    taxbenDefaultValues();

    $(".info").info();

    /* IE fix : label with img click */
    if (navigator.userAgent.indexOf("MSIE ") > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        $("label img", $(".taxben-form")).on("click", function () {
            $("#" + $(this).parents("label").attr("for")).click();
        });
    }

    initStep0($(".taxben-form .step-0"));
    initStep1($(".taxben-form .step-1"));
}

function taxbenDefaultValues() {

    Form._private = {};

    Form.Output = "Country";

    Form.Countries = [];
    Form.Years = [];

    Form.Couple = true;
    Form.AdultsAge = 40;
    Form.Children = 2;
    Form.ChildrenAges = [4, 6];
    Form._private.ChildrenAges = [4, 6, 8, 10];

    Form.AmountCountry = null;

    Form.Adult1OutputBy = null;
    Form._private.Adult1OutputBy = "earnings";
    Form.Adult1Wage = null;
    Form._private.Adult1Wage = "Percent";
    Form.Adult1Amount = null;
    
    Form.Adult1Employed = undefined;
    Form._private.Adult1Employed = true;
    Form.Adult1Earnings = undefined;
    Form._private.Adult1Earnings = 100;
    Form.Adult1WorkingTime = undefined;
    Form._private.Adult1WorkingTime = 100;
    Form.CurrentJob = undefined;
    var y, m;
    Form._private.CurrentJob = 0.1675 * 12;
    y = Math.floor(Form._private.CurrentJob / 12);
    m = Form._private.CurrentJob % 12;
    $("#months-to-years1").text("(" + (y === 0 ? "" : (y + " year" + (y === 1 ? "" : "s"))) +
        (m === 0 ? "" : (y === 0 ? "" : " and ") + m + " month" + (m === 1 ? "" : "s")) + ")");
    if (y === 0 && m === 0) $("#months-to-years1").html("&nbsp;");
    Form.SocialContribution = null;
    Form._private.SocialContribution = 22 * 12;
    y = Math.floor(Form._private.SocialContribution / 12);
    m = Form._private.SocialContribution % 12;
    $("#months-to-years2").text("(" + (y === 0 ? "" : (y + " year" + (y === 1 ? "" : "s"))) +
        (m === 0 ? "" : (y === 0 ? "" : " and ") + m + " month" + (m === 1 ? "" : "s")) + ")");
    if (y === 0 && m === 0) $("#months-to-years2").html("&nbsp;");
    Form.MonthsOutOfWork = undefined;
    Form._private.MonthsOutOfWork = [2];
    Form._private.MonthsOutOfWork_range = [1, 60];

    Form.Adult2Wage = null;
    Form._private.Adult2Wage = "Percent";
    Form.Adult2Amount = null;

    Form.Adult2Employed = false;
    Form.Adult2Earnings = null;
    Form._private.Adult2Earnings = 67;
    Form.Adult2WorkingTime = null;
    Form._private.Adult2WorkingTime = 100;

    Form.SocialAssistance = true;
    Form.HousingBenefit = false;
    Form.UnemploymentBenefit = null;
    Form.NewJobBenefit = undefined;
    Form._private.NewJobBenefit = false;

    Form.InWorkBenefit = true;
    Form.FamilyBenefit = true;
    Form.HomecareBenefit = true;
    Form.ChildcareBenefit = null;
    Form.AloneParentBenefit = null;

    Form.Adult1PreviousWage = null;
    Form._private.Adult1PreviousWage = "Percent";
    Form.Adult1PreviousEarnings = null;
    Form._private.Adult1PreviousEarnings = 100;
    Form.Adult1PreviousAmount = null;

    Form.HousingBenefitType = null;
    Form._private.HousingBenefitType = "Percent";
    Form.AnnualHousingCosts = undefined;
    Form._private.AnnualHousingCosts = 20;
    Form.HousingAmount = null;
}

function resetTaxbenForm() {
    OnWizardStepChange = null;
    $(".taxben-result-button-container-top").empty();
    taxbenDefaultValues();
    Wizard.wizard("index", 1);
    Wizard.find(".wizard-path-step").each(function (i) {
        $(this).prop("disabled", i > 1).toggleClass("disabled", i > 1);
    });
}

function transitionTaxbenForm(from, to) {
    var context = $(".taxben-form .step-" + to);
    switch (to) {
        case 0: initStep0(context); break;
        case 1: initStep1(context); break;
        case 2: initStep2(context); break;
        case 3: initStep3(context); break;
        case 4: initStep4(context); break;
        case 5: initStep5(context); break;
        case 6: initStep6(context); break;
    }
    if (to > 0) $(".wizard-buttons-container").show();
}

function changeChecks(step) {

    var ctryCbs = $(".taxben-form .step-2 .country-list input[type=checkbox]");

    if (!Form.Countries.length) {
        Form.Countries = Form.Output != "Year" 
            ? ctryCbs.map(function (i, cb) { var country = $(cb).attr("data-taxben-value"); return $.inArray(country, Config.excludedCountries) > -1 ? null : country; }).get()
            : [ctryCbs.eq(0).attr("data-taxben-value")];
    }

    var yearCbs = $(".taxben-form .step-2 .year-list input[type=checkbox]");
    if (Form.Output === "Year") {
        Form.Years = (Form.Years.length && step !== 1 ? Form.Years :
            yearCbs.not(":disabled").map(function (i, cb) { return $(cb).attr("data-taxben-value"); }).get());
    }
    else {
        Form.Years = (Form.Years.length ? [Form.Years[Form.Years.length - 1]] : [yearCbs.not(":disabled").last().attr("data-taxben-value")]);
    }

    if (Form.Output === "WorkingTime") Form.Adult1Employed = true;
    else if (Form.Output === "Unemployment") Form.Adult1Employed = false;
    else Form.Adult1Employed = (Form.Adult1Employed !== undefined ? Form.Adult1Employed : true);

    if (Form.Adult1Employed)
    {
        Form.Adult1Wage = Form.Adult1Wage != null ? Form.Adult1Wage : Form._private.Adult1Wage;
        Form.Adult1Earnings = Form.Adult1Earnings != null ? Form.Adult1Earnings : Form._private.Adult1Earnings;
    }
    else {
        Form.Adult1Earnings = null;
        Form.Adult1WorkingTime = null;
    }

    if (!Form.Couple) {
        Form.Adult2Employed = null;
        Form.Adult2Wage = null;
        Form.Adult2Amount = null;
    }

    Form.Adult2Earnings = (Form.Adult2Employed ? (Form.Adult2Earnings != null ? Form.Adult2Earnings : Form._private.Adult2Earnings) : null);
    Form.Adult2WorkingTime = (Form.Adult2Employed ? (Form.Adult2WorkingTime != null ? Form.Adult2WorkingTime : Form._private.Adult2WorkingTime) : null);

    if (Form.Adult1Employed && !Form.NewJobBenefit) 
    {
        Form.MonthsOutOfWork = null;
    }
    else if (Form.Output === "Unemployment") 
    {
        Form.MonthsOutOfWork = (Form.MonthsOutOfWork && Form.MonthsOutOfWork.length === 2) ?
            Form.MonthsOutOfWork :
            Form._private.MonthsOutOfWork_range;
    }
    else if(!Form.Adult1Employed) 
    {
        Form.MonthsOutOfWork = (Form.MonthsOutOfWork && Form.MonthsOutOfWork.length === 1) ?
            Form.MonthsOutOfWork :
            Form._private.MonthsOutOfWork;
    }

    Form.HousingBenefit = Form.HousingBenefit != null ? Form.HousingBenefit : Form._private.HousingBenefit;
    Form.AnnualHousingCosts = (Form.HousingBenefit ? (Form.AnnualHousingCosts || Form._private.AnnualHousingCosts) : null);

    if (Form.Adult1Employed) 
    {
        Form.CurrentJob = Form.CurrentJob != null ? Form.CurrentJob : Form._private.CurrentJob;
        Form.NewJobBenefit = Form.NewJobBenefit != null ? Form.NewJobBenefit : Form._private.NewJobBenefit;
    }
    else 
    {
        Form.CurrentJob = null;
        Form.NewJobBenefit = null;
    }

    if (!Form.NewJobBenefit) Form.CurrentJob = null;
}

function initStep0(context) {

    $("#wizard-run-button,#wizard-next-button,#wizard-prev-button").hide();
    $("#wizard-reset-button").css("visibility", "hidden");
    $(".wizard-buttons-container").hide();
    changeChecks(0);
}

function initStep1(context) {

    // Init
    $("input[type=radio]", context)
        .prop("checked", false)
        .filter("[data-taxben-value=" + Form.Output + "]")
        .prop("checked", true);
    changeChecks(1);
    if (!context.data("wired")) {
        $("input[type=radio]", context).on("click", function () {
            _step1Change();
        });
    }
    // Buttons
    $(".taxben-form .wizard-buttons-container input").show();
    $("#wizard-reset-button").css("visibility", "visible");

    context.data("wired", true);

    // Info
    $(".taxben-form .output").text($("input[type=radio]:checked", context).parent().attr("title"));

    // Path
    Wizard.find(".wizard-path-step.disabled").prop("disabled", Form.Output === undefined)
        .toggleClass("disabled", Form.Output === undefined);


    // Change
    function _step1Change() {
        // Set
        taxbenDefaultValues();
        Form.Output = $("input[type=radio]:checked", context).attr("data-taxben-value");

        // Info
        $(".taxben-form .output").text($("input[type=radio]:checked", context).parent().attr("title"));

        // Path
        Wizard.find(".wizard-path-step").prop("disabled", false).removeClass("disabled");

        // Next steps
        changeChecks(1);
        Wizard.wizard("index", 2);
    }
}

function initStep2(context) {
  
    // Init
    //// Checkboxes
    var ctryCbs = initCountries(context, _step2Change);
    var yearCbs = initYears(context, _step2Change);
    context.data("wired", true);

    // Buttons
    $(".taxben-form .wizard-buttons-container input").show();

    var hintCountries = Form.Output === "Year"
        ? "(only one option available for this output type)"
        : "(multiple options available)";

    $("#mult-countries", context).text(hintCountries);

    var hintYears = Form.Output !== "Year"
        ? "(only one option available for this output type)"
        : "(multiple options available)";

    $("#mult-years", context).text(hintYears);

    OnWizardStepChange = function (callback) {

        var notValid = {};
        var notValidCount = 0;

        // Check not available combinations of country + year
        Form.Countries.forEach(function (country) {
            Form.Years.forEach(function (year) {
                if (!Config.availability[country][year]) {
                    notValidCount++;
                    if (notValid[country]) notValid[country].push(year);
                    else notValid[country] = [year];
                }
            });
        });

        if (notValidCount === 0) {
            callback();
            return;
        }
        
        var allInvalid = notValidCount === Form.Countries.length * Form.Years.length;

        var template = allInvalid
            ? Config.warnings.allSelectedCountriesNotAvailable
            : Config.warnings.partiallySelectedCountriesNotAvailable;

        var msgArg = $.map(notValid, function (years, country) { return CountryMap[country] + " (" + years.join(", ") + ")"; }).join(", ");

        invokeWarning(!allInvalid, template.replace(/%s/, msgArg), callback);
    }

    // Change
    function _step2Change() {
        // One only
        if (Form.Output === "Year" && $(this).parent().parent().hasClass("country-list")) {
            ctryCbs.prop("checked", false);
            $(this).prop("checked", true);
        }

        if (Form.Output !== "Year" && $(this).parent().parent().hasClass("year-list")) {
            yearCbs.prop("checked", false);
            $(this).prop("checked", true);
        }

        // Set
        Form.Countries = ctryCbs.filter(":checked").map(function (i, cb) { return $(cb).attr("data-taxben-value"); }).get();
        Form.Years = yearCbs.filter(":checked").map(function (i, cb) { return $(cb).attr("data-taxben-value"); }).get();
    }

    function initCountries(context, change) {

        var ctryCbs = $(".country-list input[type=checkbox]", context);

        ctryCbs.each(function () {
            $(this).prop("checked", ($.inArray($(this).attr("data-taxben-value"), Form.Countries) > -1));
        });

        if (!context.data("wired")) {
            ctryCbs.on("click", change);

            $("a#ctryAll", context).on("click", function () {
                ctryCbs.prop("checked", true);
                change();
            });
            $("a#ctryNone", context).on("click", function () {
                ctryCbs.prop("checked", false);
                change();
            });
            $("a#ctryOECD", context).on("click", function () {
                ctryCbs.prop("checked", false)
                    .each(function () {
                        $(this).prop("checked", $.inArray($(this).attr("data-taxben-value"), notOECD) === -1);
                    });
                change();
            });
            $("a#ctryEU27", context).on("click", function () {
                ctryCbs.prop("checked", false)
                    .each(function () {
                        $(this).prop("checked", $.inArray($(this).attr("data-taxben-value"), notUE27) === -1);
                    });
                change();
            });
        }
        $("a#ctryAll,a#ctryNone,a#ctryOECD,a#ctryEU27", context).toggle(Form.Output !== "Year");

        return ctryCbs;
    }

    function initYears(context, change) {

        var yearCbs = $(".year-list input[type=checkbox]", context);

        yearCbs.each(function () {
            $(this).prop("checked", ($.inArray($(this).attr("data-taxben-value"), Form.Years) > -1));
        });

        if (!context.data("wired")) {
            yearCbs.on("click", change);

            $("a#yearAll", context).on("click", function () {
                yearCbs.not(":disabled").prop("checked", true);
                change();
            });
            $("a#yearNone", context).on("click", function () {
                yearCbs.prop("checked", false);
                change();
            });
        }

        $("a#yearAll,a#yearNone", context).toggle(Form.Output === "Year");

        return yearCbs;
    }

    // Buttons
    //$(".taxben-form #wizard-next-button,.taxben-form #wizard-run-button").toggleClass("disabled", !Form.Countries.length || !Form.Years.length);

    $("#wizard-run-button,#wizard-next-button").show();
    $("#wizard-reset-button").css("visibility", "visible");
}

function initStep3(context) {

    // Init family type
    $("#familyCouple", context).prop("checked", Form.Couple);
    $("#familySingle", context).prop("checked", !Form.Couple);
    // Change family type
    if (!context.data("wired")) {
        $("#familySingle,#familyCouple", context).on("change", function () {
            Form.Couple = $("#familyCouple", context).is(":checked");

            changeChecks();
        });
    }
    context.data("wired", true);

    // Init + change adults age
    if (!$("#adultsAge", context).hasClass("ui-slider")) {
        $("#adultsAge", context)
            .slider({
                min: 19,
                max: 60,
                change: function (event, ui) {
                    $(".ui-slider-handle", $(this)).text(ui.value);
                    Form.AdultsAge = ui.value;
                },
                slide: function (event, ui) {
                    $(".ui-slider-handle", $(this)).text(ui.value);
                    Form.AdultsAge = ui.value;
                }
            });
    }
    $("#adultsAge", context).slider("value", Form.AdultsAge);

    // Init + change children nb
    if (!$("#familyChildren", context).hasClass("ui-slider")) {
        $("#familyChildren", context)
            .slider({
                min: 0,
                max: 4,
                change: _slide2,
                slide: _slide2
            });
        function _slide2(event, ui) {
            $(".ui-slider-handle", $(this)).text(ui.value);
            Form.Children = ui.value;
            if (ui.value > 0) {
                $(".children-ages", context).slideDown(100);
            }
            else {
                $(".children-ages", context).slideUp(100);
            }
            if (ui.value === 0) Form.ChildrenAges = null;
            else {
                Form.ChildrenAges = [];
                $(".child-age", context).each(function (i) {
                    if (i + 1 <= ui.value) {
                        $(this).slideDown(100);
                        Form.ChildrenAges[i] = $(this).slider("value");
                    }
                    else {
                        $(this).slideUp(100);
                        delete (Form.ChildrenAges[i]);
                    }
                });
            }
        }
    }

    // Init + change children ages
    for (var i = 0; i < Form._private.ChildrenAges.length;i++) {
        Form.ChildrenAges = Form.ChildrenAges || Form._private.ChildrenAges;
        if (!$(".child-age", context).eq(i).hasClass("ui-slider")) {
            $(".child-age", context)
                .eq(i)
                .data("index", i)
                .slider({
                    min: 2,
                    max: 17,
                    change: function (event, ui) {
                        $(".ui-slider-handle", $(this)).text(ui.value);
                        Form.ChildrenAges[parseInt($(this).data("index"))] = ui.value;
                    },
                    slide: function (event, ui) {
                        $(".ui-slider-handle", $(this)).text(ui.value);
                        Form.ChildrenAges[parseInt($(this).data("index"))] = ui.value;
                    }
                });
        }

        if (i + 1 <= Form.Children) {
            $(".child-age", context).eq(i).show().slider("value", Form.ChildrenAges[i]);
        }
        else {
            $(".child-age", context).eq(i).hide().slider("value", Form._private.ChildrenAges[i]);
        }
    }
    $("#familyChildren", context).slider("value", Form.Children);

    $("#wizard-run-button,#wizard-next-button").show();
    $("#wizard-reset-button").css("visibility", "visible");
}

function initStep4(context) {

    // Init employments
    $("#adult1Employed", context).prop("checked", Form.Adult1Employed);
    $("#adult1OutOfWork", context).prop("checked", !Form.Adult1Employed);
    $("#adult1Employed", context).prop("disabled", Form.Output === "Unemployment");
    $("#info-employed", context).toggle(Form.Output === "WorkingTime");
    $("#adult1OutOfWork", context).prop("disabled", Form.Output === "WorkingTime");
    $("#info-adult1Employed", context).toggle(Form.Output !== "Unemployment");
    $("#info-outofwork", context).toggle(Form.Output === "Unemployment");
    $("#monthsOutOfWork-container", context).toggle(Form.Output !== "Unemployment");

    if (Form.Couple) {
        $(".when-couple", context).show();
        $("#adult2Employed", context).prop("checked", Form.Adult2Employed);
        $("#adult2OutOfWork", context).prop("checked", !Form.Adult2Employed);
        $(".when-partneremployed", context).toggle(Form.Adult2Employed);
    }
    else {
        $(".when-couple", context).hide();
    }
    $(".when-employed", context).toggle(Form.Adult1Employed);
    $(".when-unemployed", context).toggle(!Form.Adult1Employed);
    $("#adult1OutputWorkingTime", context).toggle(Form.Output === "WorkingTime");

    // -------------------------------------------------------------
    var $adult1EarningsSlider = $("#adult1Earnings", context);
    var $adult1WorkingTimeSlider = $("#adult1WorkingTime", context);
    var $monthsOutOfWorkSlider = $("#monthsOutOfWork", context);
    var $monthsOutOfWorkSliderRange = $("#monthsOutOfWork-range", context);
    var $adult1Amount = $('#adult1_Amount', context);
    var $adult1Country = $('#adult1_CountryIsoCode', context);

    var $adult2EarningsSlider = $("#adult2Earnings", context);
    var $adult2WorkingTimeSlider = $("#adult2WorkingTime", context);
    var $adult2Amount = $('#adult2_Amount', context);
    var $adult2Country = $('#adult2_CountryIsoCode', context);

    $adult1Country.find('option').remove();
    $adult2Country.find('option').remove();
    Form.Countries.forEach(function(countryCode) {
        $adult1Country.append('<option value=' + countryCode + '>' + CountryMap[countryCode] + '</option>');
        $adult2Country.append('<option value=' + countryCode + '>' + CountryMap[countryCode] + '</option>');
    });

    // -----------------------------------------

    // Change employment 1
    if (!context.data("wired")) {

        $adult1Amount.ForceNumericOnly();
        $adult2Amount.ForceNumericOnly();

        // Earnings 1 slider -------------------------

        $adult1EarningsSlider.slider({
            min: Form.Adult1Employed ? 1 : 0,
            max: 200,
            value: Form._private.Adult1Earnings,
            create: function () {
                $(".ui-slider-handle", $(this)).width(38).text($(this).slider("value"));
            },
            change: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.Adult1Earnings = ui.value;
            },
            slide: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.Adult1Earnings = ui.value;
            }
        });

        // Worktime 1 slider
        $adult1WorkingTimeSlider.slider({
            min: 1,
            max: 100,
            value: Form._private.Adult1WorkingTime,
            create: function () {
                $(".ui-slider-handle", $(this)).width(38).text($(this).slider("value"));
            },
            change: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.Adult1WorkingTime = ui.value;
            },
            slide: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.Adult1WorkingTime = ui.value;
            }
        });

        // Months out of Work single slider
        $monthsOutOfWorkSlider.slider({
            min: 1,
            max: 60,
            value: Form._private.MonthsOutOfWork,
            create: function () {
                $(".ui-slider-handle", $(this)).text($(this).slider("value"));
            },
            change: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.MonthsOutOfWork = [ui.value];
            },
            slide: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.MonthsOutOfWork = [ui.value];
            }
        });

        // Months out of Work single range slider
        var handle21 = $("#monthsOutOfWork-range-handle1", context);
        var handle22 = $("#monthsOutOfWork-range-handle2", context);
        handle21.on("mousedown", function () { $(this).css("z-index", 3); handle22.css("z-index", 2); });
        handle22.on("mousedown", function () { $(this).css("z-index", 3); handle21.css("z-index", 2); });
        $monthsOutOfWorkSliderRange.slider({
            range: true,
            min: 1,
            max: 60,
            values: Form._private.MonthsOutOfWork_range,
            create: function () {
                handle21.text($(this).slider("values", 0));
                handle22.text($(this).slider("values", 1));
            },
            slide: function (event, ui) {
                if (ui.values[1] - ui.values[0] < 5) {
                    $(this).slider("option", "values", Form.MonthsOutOfWork);
                    return false;
                }
                handle21.text(ui.values[0]);
                handle22.text(ui.values[1]);
                Form.MonthsOutOfWork = [ui.values[0], ui.values[1]];
            }
        });

        $adult1Amount.on('keyup change', function () {
            $adult1Country.prop("disabled", isEmpty(this.value));
            if (isEmpty(this.value)) { $adult1Country.val([]); }
        });

        $adult1Country.on('change', function () {
            $adult2Country.val(this.value)
        });

        $adult2Country.on('keyup change', function () {
            $adult1Country.val(this.value)
        });
        

        // Adult 2 -------------------------------------------

        $adult2EarningsSlider.slider({
            min: 1,
            max: 200,
            value: Form._private.Adult2Earnings,
            create: function () {
                $(".ui-slider-handle", $(this)).width(38).text($(this).slider("value"));
            },
            change: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.Adult2Earnings = ui.value;
            },
            slide: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.Adult2Earnings = ui.value;
            }
        });

        $adult2WorkingTimeSlider.slider({
            min: 1,
            max: 100,
            value: Form._private.Adult2WorkingTime,
            create: function () {
                $(".ui-slider-handle", $(this)).width(38).text($(this).slider("value"));
            },
            change: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.Adult2WorkingTime = ui.value;
            },
            slide: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.Adult2WorkingTime = ui.value;
            }
        });

        $adult2Amount.on('change', function () {
            $adult2Country.prop("disabled", isEmpty(this.value));
            if (isEmpty(this.value)) { $adult2Country.val([]);}
        });

        $("#adult1Employed,#adult1OutOfWork", context).on("change", function () {
            // Set
            Form.Adult1Employed = $("input#adult1Employed", context).is(":checked");

            // Employment 1 dependency
            if (Form.Adult1Employed) {
                $(".when-employed", context).slideDown(200);
                $(".when-unemployed", context).slideUp(200);
            }
            else {
                $(".when-employed", context).slideUp(200);
                $(".when-unemployed", context).slideDown(200);
            }

            $adult1EarningsSlider.slider("option", "min", Form.Adult1Employed ? 1 : 0);

            // when unemployed Adult1Earnings min is 0, when employed 1
            if (Form.Adult1Employed && Form.Adult1Earnings === 0) {
                Form.Adult1Earnings = 1;
                $adult1EarningsSlider.slider("option", "value", 1);
            }

            // when unemployed Adult1Amount min is 0, when employed 1
            if (Form.Adult1Employed && Form.Adult1Amount === "0") {
                $adult1Amount.val("").change();
            }

            changeChecks();
        });

        // Change employment 2
        $("#adult2Employed,#adult2OutOfWork").on("change", function () {
            // Set
            Form.Adult2Employed = (Form.Couple ? $("input#adult2Employed", context).is(":checked") : null);

            // Employment 2 dependency
            if (Form.Adult2Employed) {
                $(".when-partneremployed", context).slideDown(200);
            }
            else {
                $(".when-partneremployed", context).slideUp(200);
            }

            changeChecks();

        });

        $('#adult1WhenEmployedRadioBtns input').on("change", function() {
            $('.wage-data div[data-taxben-value]', context).hide();
            $('.wage-data div[data-taxben-value=' + this.value + ']', context).show();
        });

        $('#adult2WhenEmployedRadioBtns input').on("change", function () {
            $('.when-partneremployed div[data-taxben-value]', context).hide();
            $('.when-partneremployed div[data-taxben-value=' + this.value + ']', context).show();
        });

        // Output WorkingTime select work hours/earnings
        $('#adult1OutputWorkingTime input', context).on("change", function () {
            Form.Adult1OutputBy = this.value;
            $('div[data-taxben-output-view]', context).hide();
            $('div[data-taxben-output-view=' + this.value + ']', context).show();
        });
    }
    context.data("wired", true);

    // Social contribution 1 
    if (Form.SocialContribution === null || Form.SocialContribution > (Form.AdultsAge - 18) * 12)
        Form.SocialContribution = (Form.AdultsAge - 18) * 12;

    $("#alert-age2", context).css("opacity", 0).find(".age").text(Form.AdultsAge);
    
    if (!$("#adult1SocialContribution", context).hasClass("ui-slider")) {
        $("#adult1SocialContribution", context).slider({
            min: 0,
            max: 41 * 12,
            step: 1,
            value: Form.SocialContribution,
            create: function () {
                $(".ui-slider-handle", $(this)).width(38).text($(this).slider("value"));
                var y = Math.floor($(this).slider("value") / 12);
                var m = $(this).slider("value") % 12;
                $("#months-to-years2").text("(" + (y === 0 ? "" : (y + " year" + (y === 1 ? "" : "s"))) +
                    (m === 0 ? "" : (y === 0 ? "" : " and ") + m + " month" + (m === 1 ? "" : "s")) + ")");
                if (y === 0 && m === 0) $("#months-to-years2").html("&nbsp;");
            },
            change: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.SocialContribution = ui.value;
                if (ui.value > (Form.AdultsAge - 18) * 12 && $("#alert-age2", context).css("opacity") == 0) {
                    $("#alert-age2", context).animate({ opacity: 1 }, 200);
                }
                else if (ui.value <= (Form.AdultsAge - 18) * 12 && $("#alert-age2", context).css("opacity") == 1) {
                    $("#alert-age2", context).animate({ opacity: 0 }, 200);
                }
                if (ui.value < Form.CurrentJob && $("#alert-ssc", context).css("opacity") == 0) {
                    $("#alert-ssc", context).animate({ opacity: 1 }, 200);
                }
                else if (ui.value >= Form.CurrentJob && $("#alert-ssc", context).css("opacity") == 1) {
                    $("#alert-ssc", context).animate({ opacity: 0 }, 200);
                }
                var y = Math.floor(ui.value / 12);
                var m = ui.value % 12;
                $("#months-to-years2").text("(" + (y === 0 ? "" : (y + " year" + (y === 1 ? "" : "s"))) +
                    (m === 0 ? "" : (y === 0 ? "" : " and ") + m + " month" + (m === 1 ? "" : "s")) + ")");
                if (y === 0 && m === 0) $("#months-to-years2").html("&nbsp;");
            },
            slide: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.SocialContribution = ui.value;
                var y = Math.floor(ui.value / 12);
                var m = ui.value % 12;
                $("#months-to-years2").text("(" + (y === 0 ? "" : (y + " year" + (y === 1 ? "" : "s"))) +
                    (m === 0 ? "" : (y === 0 ? "" : " and ") + m + " month" + (m === 1 ? "" : "s")) + ")");
                if (y === 0 && m === 0) $("#months-to-years2").html("&nbsp;");
            }
        });
    }
    if (Form.AdultsAge)
    {
        Form._private.SocialContribution = (Form.AdultsAge - 18) * 12;
    }
    $("#adult1SocialContribution", context).slider("value", Form.SocialContribution === null ? Form._private.SocialContribution : Form.SocialContribution);

    // re-Load data from saved Form

    if (Form.Output !== "Unemployment") 
    {
        $monthsOutOfWorkSlider.slider("value",
            ((Form.MonthsOutOfWork && Form.MonthsOutOfWork.length === 1) ?
            Form.MonthsOutOfWork[0] : Form._private.MonthsOutOfWork[0]))
            .show();

        $("#monthsOutOfWork-range-cont", context).hide();
        $("#info-outofwork-range").hide();
    }
    else 
    {
        $("#monthsOutOfWork-range-cont", context).show();
        $("#info-outofwork-range").show();

        $monthsOutOfWorkSliderRange
            .slider("values", 0, ((Form.MonthsOutOfWork && Form.MonthsOutOfWork.length === 2) ?
                Form.MonthsOutOfWork[0] : Form._private.MonthsOutOfWork_range[0]))
            .slider("values", 1, ((Form.MonthsOutOfWork && Form.MonthsOutOfWork.length === 2) ?
                Form.MonthsOutOfWork[1] : Form._private.MonthsOutOfWork_range[1]));

        $monthsOutOfWorkSlider.hide();
    }

    if (Form.Adult1Employed && Form.Output === "WorkingTime") 
    {
        $('div[data-taxben-output-view]', context).hide();
        $('#adult1OutputWorkingTime input', context).prop("checked", false);
        $('#adult1OutputWorkingTime input[value=' + (Form.Adult1OutputBy || Form._private.Adult1OutputBy) + ']', context).click();
    }

    $adult1WorkingTimeSlider.slider("value", Form.Adult1WorkingTime || Form._private.Adult1WorkingTime);

    if (Form.Couple && Form.Adult2Employed) 
    {
        $adult2EarningsSlider.slider("value", Form.Adult2Earnings || Form._private.Adult2Earnings);
        $adult2WorkingTimeSlider.slider("value", Form.Adult2WorkingTime || Form._private.Adult2WorkingTime);
    }

    $adult1Country.val([Form.AmountCountry]);
    Form.AmountCountry = $adult1Country.val();
    $adult1Country.prop("disabled", isEmpty(Form.Adult1Amount));

    $adult2Country.val([Form.AmountCountry]);
    $adult2Country.prop("disabled", isEmpty(Form.Adult2Amount));

    $adult1Amount.val(Form.Adult1Amount);
    $adult2Amount.val(Form.Adult2Amount);

    // Save to Form on step change
    OnWizardStepChange = function(callback) {
     
        Form.Adult1OutputBy = null;

        // For Adult 1 Employed/Unemployed
        Form.Adult1Wage = null;
        Form.Adult1Earnings = null;
        Form.Adult1Amount = null;
        Form.Adult1WorkingTime = null;

        // For Adult1 Unemployed
        Form.MonthsOutOfWork = null;
        Form.SocialContribution = null;

        Form.Adult2Wage = null;
        Form.Adult2Earnings = null;
        Form.Adult2Amount = null;
        Form.Adult2WorkingTime = null;

        var isFormValid = true;

        // Unemployed
        if(!Form.Adult1Employed) 
        {
            Form.MonthsOutOfWork = Form.Output !== "Unemployment"
                ? [$monthsOutOfWorkSlider.slider('value')]
                : [$monthsOutOfWorkSliderRange.slider('values', 0), $monthsOutOfWorkSliderRange.slider('values', 1)];
            
            Form.SocialContribution = $("#adult1SocialContribution", context).slider('value');
        }

        // Employed
        if (Form.Adult1Employed ) 
        {
            Form.Adult1OutputBy = Form.Output === "WorkingTime" 
                ? $('#adult1OutputWorkingTime input[type=radio]:checked', context).val() 
                : null;

            if (Form.Adult1OutputBy === null || Form.Adult1OutputBy === "workdayp") 
            {
                Form.Adult1Wage = $('#adult1WhenEmployedRadioBtns input[type=radio]:checked', context).val();

                if (Form.Adult1Wage === "Percent")
                {
                    Form.Adult1Earnings = $adult1EarningsSlider.slider('value');
                }
                else if (Form.Adult1Wage === "Amount")
                {
                    Form.Adult1Amount = $adult1Amount.val();
                    Form.AmountCountry = Form.Adult1Amount
                        ? $adult1Country.val() || Form.AmountCountry || Form.Countries[0]
                        : Form.AmountCountry;

                    isFormValid &= checkInputNotEmpty($adult1Amount);
                    isFormValid &= checkInputNotEmpty($adult1Country);
                }
            }

            if (Form.Adult1OutputBy === null || Form.Adult1OutputBy === "earnings") 
            {
                Form.Adult1WorkingTime = $adult1WorkingTimeSlider.slider('value');
            }
        }

        if (Form.Couple && Form.Adult2Employed) 
        {
            Form.Adult2Wage = $('#adult2WhenEmployedRadioBtns input[type=radio]:checked', context).val();

            if (Form.Adult2Wage === "Percent")
            {
                Form.Adult2Earnings = $adult2EarningsSlider.slider('value');
            }
            else if (Form.Adult2Wage === "Amount")
            {
                Form.Adult2Amount = $adult2Amount.val();
                Form.AmountCountry = Form.Adult2Amount
                    ? $adult2Country.val() || Form.AmountCountry || Form.Countries[0]
                    : Form.AmountCountry;

                isFormValid &= checkInputNotEmpty($adult2Amount);
                isFormValid &= checkInputNotEmpty($adult2Country);
            }

            Form.Adult2WorkingTime = $adult2WorkingTimeSlider.slider('value');
        }

        // ---------------------------

        if (!isFormValid)
            return;

        // ---------------------------

        var notValid = {};
        var notValidCount = 0;

        // Check if there country + year, where there is no minimum wage
        if (Form.Adult1Wage === "Minimum" || Form.Adult2Wage === "Minimum") {
            Form.Countries.forEach(function (country) {
                Form.Years.forEach(function (year) {
                    var comboConfig = Config.availability[country][year];
                    if (!comboConfig || comboConfig.minWage === 0) {
                        notValidCount++;
                        if (notValid[country]) notValid[country].push(year);
                        else notValid[country] = [year];
                    }
                });
            });
        }

        if (notValidCount === 0) {            
            callback();
            return;
        }

        var allInvalid = notValidCount === Form.Countries.length * Form.Years.length;
        var template = allInvalid
            ? Config.warnings.allSelectedCountriesHaveNoMinimumWage
            : Config.warnings.partiallySelectedCountriesHaveNoMinimumWage;
        
        var msgArg = $.map(notValid, function (years, country) {return CountryMap[country] + " (" + years.join(", ") + ")";}).join(", ");

        invokeWarning(!allInvalid, template.replace(/%s/, msgArg), callback);

        // -------------------------
    }

    $("#wizard-run-button,#wizard-next-button").show();
    $("#wizard-reset-button").css("visibility", "visible");
}

function initStep5(context) {

    var $previousEarningsSlider = $("#benefit-unemployment-earnings", context);
    var $housingCostsSlider = $("#housingCosts", context);
    var $monthsOfNewJobSlider = $("#monthsOfNewJob", context);
    var $monthsOutOfWorkSlider = $("#monthsOutOfWork2", context);

    Form.UnemploymentBenefit = Form.UnemploymentBenefit === null
        ? !Form.Adult1Employed
        : Form.UnemploymentBenefit;
    
    // Init and toggle
    $("#unemploymentBenefit", context)
        .prop("checked", Form.UnemploymentBenefit)
        .parents("tr").find("td:nth-child(2)>div").toggle(Form.UnemploymentBenefit);

    $("#benefit-unemployment-form", context)
        .parent()
        .toggle(Form.Adult1Employed || Form.SocialContribution > 0)

    $("#socialAssistance", context).prop("checked", Form.SocialAssistance);
    $("#housingBenefit", context).prop("checked", Form.HousingBenefit);    
    $("#inWorkBenefit", context).prop("checked", Form.InWorkBenefit);
    $("#familyBenefit", context).prop("checked", Form.FamilyBenefit);

    var showHomecareBenefit = Form.Children > 0;

    $("#homecareBenefit", context)
        .prop("checked", showHomecareBenefit && Form.HomecareBenefit)
        .parents("tr").find("td").toggle(showHomecareBenefit);

    var showChildcareBenefit = Form.Children > 0 &&
        Form.ChildrenAges.every(function (age) { return age < 5 }) &&
        Form.Adult1Employed === true &&
        Form.Adult1WorkingTime == 100 &&
        (!Form.Couple || (Form.Adult2Employed === true && Form.Adult2WorkingTime == 100));

    $("#childcareBenefit", context)
        .prop("checked", showChildcareBenefit && Form.ChildcareBenefit)
        .prop("disabled", !showChildcareBenefit)
        .parents("tr").find("td")
            .toggle(Form.Children > 0);

    var showAloneParentBenefit = !Form.Couple && Form.Children > 0;

    $("#aloneParentBenefit", context)
        .prop("checked", showAloneParentBenefit && Form.AloneParentBenefit)
        .parents("tr").find("td").toggle(showAloneParentBenefit);
    
    if (Form.NewJobBenefit === null) {
        $("#newJobBenefit", context).parents("tr").find("td").hide();
    }
    else {
        $("#newJobBenefit", context).prop("checked", Form.NewJobBenefit)
            .parents("tr").find("td").show();
    }

    $(".when-jobbenefit", context).toggle(Form.NewJobBenefit);

    var $unemploymentAmount = $('#benefit-unemployment-amount', context);
    var $unemploymentCountry = $('#benefit-unemployment-country', context);

    var $housingAmount = $('#benefit-housing-amount', context);
    var $housingCountry = $('#benefit-housing-country', context);

    $unemploymentCountry.find('option').remove();
    $housingCountry.find('option').remove();
    Form.Countries.forEach(function (countryCode) {
        $unemploymentCountry.append('<option value=' + countryCode + '>' + CountryMap[countryCode] + '</option>');
        $housingCountry.append('<option value=' + countryCode + '>' + CountryMap[countryCode] + '</option>');
    });

    $unemploymentCountry.val([Form.AmountCountry]);
    $unemploymentCountry.prop("disabled", isEmpty(Form.Adult1PreviousAmount));

    $housingCountry.val([Form.AmountCountry]);
    $housingCountry.prop("disabled", isEmpty(Form.HousingAmount));

    $unemploymentAmount.val(Form.Adult1PreviousAmount);
    $housingAmount.val(Form.HousingAmount);

    //$('#benefit-unemployment-form input[type=radio]')

    // Change
    if (!context.data("wired")) {

        $previousEarningsSlider.slider({
            min: 1,
            max: 200,
            value: Form._private.Adult1PreviousEarnings,
            create: function () {
                $(".ui-slider-handle", $(this)).text($(this).slider("value"));
            },
            change: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.Adult1PreviousEarnings = ui.value;
            },
            slide: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.Adult1PreviousEarnings = ui.value;
            }
        });

        $housingCostsSlider.slider({
            min: 1,
            max: 100,
            value: Form._private.AnnualHousingCosts,
            create: function () {
                $(".ui-slider-handle", $(this)).text($(this).slider("value"));
            },
            change: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.AnnualHousingCosts = ui.value;
            },
            slide: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.AnnualHousingCosts = ui.value;
            }
        });

        $monthsOfNewJobSlider.slider({
            min: 1,
            max: 1 * 12,
            value: Form.CurrentJob,
            create: function () {
                $(".ui-slider-handle", $(this)).text($(this).slider("value"));
                var y = Math.floor($(this).slider("value") / 12);
                var m = $(this).slider("value") % 12;
                $("#months-to-years1").text("(" + (y === 0 ? "" : (y + " year" + (y === 1 ? "" : "s"))) +
                    (m === 0 ? "" : (y === 0 ? "" : " and ") + m + " month" + (m === 1 ? "" : "s")) + ")");
                if (y === 0 && m === 0) $("#months-to-years1").html("&nbsp;");
            },
            change: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.CurrentJob = ui.value;
                
                $("#alert-age1", context).toggle(ui.value > (Form.AdultsAge - 18) * 12);

                if (ui.value > Form.SocialContribution && $("#alert-ssc", context).css("opacity") == 0) {
                    $("#alert-ssc", context).animate({ opacity: 1 }, 200);
                }
                else if (ui.value <= Form.SocialContribution && $("#alert-ssc", context).css("opacity") == 1) {
                    $("#alert-ssc", context).animate({ opacity: 0 }, 200);
                }
                var y = Math.floor(ui.value / 12);
                var m = ui.value % 12;
                $("#months-to-years1").text("(" + (y === 0 ? "" : (y + " year" + (y === 1 ? "" : "s"))) +
                    (m === 0 ? "" : (y === 0 ? "" : " and ") + m + " month" + (m === 1 ? "" : "s")) + ")");
                if (y === 0 && m === 0) $("#months-to-years1").html("&nbsp;").css("marginTop", 5);
            },
            slide: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.CurrentJob = ui.value;
                var y = Math.floor(ui.value / 12);
                var m = ui.value % 12;
                $("#months-to-years1").text("(" + (y === 0 ? "" : (y + " year" + (y === 1 ? "" : "s"))) +
                    (m === 0 ? "" : (y === 0 ? "" : " and ") + m + " month" + (m === 1 ? "" : "s")) + ")");
                if (y === 0 && m === 0) $("#months-to-years1").html("&nbsp;").css("marginTop", 5);
            }
        });

        $monthsOutOfWorkSlider.slider({
            min: 1,
            max: 60,
            value: Form.MonthsOutOfWork || Form._private.MonthsOutOfWork,
            create: function () {
                $(".ui-slider-handle", $(this)).text($(this).slider("value"));
            },
            change: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.MonthsOutOfWork = [ui.value];
            },
            slide: function (event, ui) {
                $(".ui-slider-handle", $(this)).text(ui.value);
                Form.MonthsOutOfWork = [ui.value];
            }
        });
        
        $("input[type=checkbox]", context).on("change", function () {
            var checked = $(this).is(":checked");
            // Set
            Form[$(this).attr("data-taxben-value")] = checked;

            // Toggle dependencies
            if (checked) {
                $(this).parents("td").next("td").find("> div").slideDown(200, function () {
                    var key = $(this).find("[data-taxben-value]").attr("data-taxben-value");
                    if (key) Form[key] = (Form._private["sav" + key] || Form._private[key]);
                });
            }
            else {
                $(this).parents("td").next("td").find("> div").slideUp(200, function () {
                    var key = $(this).find("[data-taxben-value]").attr("data-taxben-value");
                    if (key) {
                        Form._private["sav" + key] = Form[key];
                        Form[key] = null;
                    }
                });
            }

            changeChecks();
        });

        $unemploymentAmount.ForceNumericOnly();
        $housingAmount.ForceNumericOnly();

        $('#benefit-unemployment-form input[type=radio]').on("change", function () {
            $('#benefit-unemployment-form div[data-taxben-value]', context).hide();
            $('#benefit-unemployment-form div[data-taxben-value=' + this.value + ']', context).show();
        });

        $('#benefit-housing-form input[type=radio]').on("change", function () {
            $('#benefit-housing-form div[data-taxben-value]', context).hide();
            $('#benefit-housing-form div[data-taxben-value=' + this.value + ']', context).show();
        });

        $unemploymentAmount.on('keyup change', function () {
            $unemploymentCountry.prop("disabled", isEmpty(this.value));
            if (isEmpty(this.value)) { $unemploymentCountry.val([]); }
        });

        $housingAmount.on('keyup change', function () {
            $housingCountry.prop("disabled", isEmpty(this.value));
            if (isEmpty(this.value)) { $housingCountry.val([]); }
        });

        $unemploymentCountry.on('change', function () {
            $housingCountry.val(this.value)
        });

        $housingCountry.on('change', function () {
            $unemploymentCountry.val(this.value)
        });

    }
    context.data("wired", true);

    if (Form.HousingBenefit)
        $housingCostsSlider.slider("value", Form.AnnualHousingCosts || Form._private.AnnualHousingCosts);

    $("#housingCosts", context).parent().toggle(Form.HousingBenefit === true);

    // Months of current job 1
    if (Form.CurrentJob === undefined) 
        Form.CurrentJob = (Form.AdultsAge - 18) * 12;
    
    $("#alert-age1", context)
        .hide()
        .find(".age")
        .text(Form.AdultsAge);
    
    if (Form.Adult1Employed) 
        $monthsOfNewJobSlider.slider("value", Form.CurrentJob || Form._private.CurrentJob);

    $("#wizard-run-button,#wizard-next-button").show();
    $("#wizard-reset-button").css("visibility", "visible");

    OnWizardStepChange = function (callback) {

        Form.Adult1PreviousWage = null;
        Form.Adult1PreviousEarnings = null;
        Form.Adult1PreviousAmount = null;
        Form.HousingBenefitType = null;
        Form.AnnualHousingCosts = null;
        Form.HousingAmount = null;

        var isFormValid = true;

        if (Form.UnemploymentBenefit && (Form.Adult1Employed || Form.SocialContribution>0))
        {
            Form.Adult1PreviousWage = $('#benefit-unemployment-form input[type=radio]:checked', context).val();

            if (Form.Adult1PreviousWage === "Percent") {
                Form.Adult1PreviousEarnings = $previousEarningsSlider.slider('value');
            }
            else if (Form.Adult1PreviousWage === "Amount") {
                Form.Adult1PreviousAmount = $unemploymentAmount.val();
                Form.AmountCountry = Form.Adult1PreviousAmount
                    ? $unemploymentCountry.val() || Form.AmountCountry || Form.Countries[0]
                    : Form.AmountCountry;

                isFormValid &= checkInputNotEmpty($unemploymentAmount);
                isFormValid &= checkInputNotEmpty($unemploymentCountry);
            }
        }

        if (Form.HousingBenefit)
        {
            Form.HousingBenefitType = $('#benefit-housing-form input[type=radio]:checked', context).val();

            if (Form.HousingBenefitType === "Percent")
            {
                Form.AnnualHousingCosts = $housingCostsSlider.slider('value');
            }
            else if (Form.HousingBenefitType === "Amount")
            {
                Form.HousingAmount = $housingAmount.val();
                Form.AmountCountry = Form.HousingAmount
                    ? $housingCountry.val() || Form.AmountCountry || Form.Countries[0]
                    : Form.AmountCountry;

                isFormValid &= checkInputNotEmpty($housingAmount);
                isFormValid &= checkInputNotEmpty($housingCountry);
            }
        }

        // ------------------------------------
        if (!isFormValid)
            return;
        // ------------------------------------

        var notValidCc = {};
        var notValidCcCount = 0;

        var notValidMin = {};
        var notValidMinCount = 0;

        // Check if there country + year, where there is no childcare
        if (Form.ChildcareBenefit) {
            Form.Countries.forEach(function (country) {
                Form.Years.forEach(function (year) {
                    var comboConfig = Config.availability[country][year];
                    if (!comboConfig || comboConfig.childcare === 0) {
                        notValidCcCount++;
                        if (notValidCc[country]) notValidCc[country].push(year);
                        else notValidCc[country] = [year];
                    }
                });
            });
        }

        if (Form.Adult1PreviousWage === "Minimum") {
            Form.Countries.forEach(function (country) {
                Form.Years.forEach(function (year) {
                    var comboConfig = Config.availability[country][year];
                    if (!comboConfig || comboConfig.minWage === 0) {
                        notValidMinCount++;
                        if (notValidMin[country]) notValidMin[country].push(year);
                        else notValidMin[country] = [year];
                    }
                });
            });
        }

        if (notValidCcCount === 0 && notValidMinCount === 0)
        {
            callback();
            return;
        }

        var msg = "";
        var allowContinue = true;

        if (notValidCcCount > 0)
        {
            var allInvalid = notValidCcCount === Form.Countries.length * Form.Years.length;

            var template = allInvalid
                ? Config.warnings.allSelectedCountriesHaveNoChildcareBenefit
                : Config.warnings.partiallySelectedCountriesNoChildcareBenefit;

            var msgArg = $.map(notValidCc, function (years, country) { return CountryMap[country] + " (" + years.join(", ") + ")"; }).join(", ");

            msg += template.replace(/%s/, msgArg) + "<br/><br/>";
            allowContinue &= !allInvalid;
        }

        if (notValidMinCount > 0)
        {
            var allInvalid = notValidMinCount === Form.Countries.length * Form.Years.length;

            var template = allInvalid
                ? Config.warnings.allSelectedCountriesHaveNoMinimumWage
                : Config.warnings.partiallySelectedCountriesHaveNoMinimumWage;

            var msgArg = $.map(notValidMin, function (years, country) { return CountryMap[country] + " (" + years.join(", ") + ")"; }).join(", ");

            msg += template.replace(/%s/, msgArg);
            allowContinue &= !allInvalid;
        }
       
        invokeWarning(allowContinue, msg, callback);
     
        // -------------------------
    }
}

function initStep6(context) {

    if (!Form.Countries.length) {
        $(".taxben-form .country-list input").each(function () {
            Form.Countries.push($(this).attr("data-taxben-value"));
        });
    }
    if (!Form.Years.length) {
        $(".taxben-form .year-list input").each(function () {
            if (!$(this).is(":disabled")) Form.Years.push($(this).attr("data-taxben-value"));
        });
    }

    if (Form.Countries.length === 1) {
        Form.CountryName = $(".taxben-form .country-list input[data-taxben-value=" + Form.Countries[0] + "]").attr("title");
    }
     
    Form.Adult1OutputBy = Form.Output !== "WorkingTime"
        ? null
        : Form.Adult1OutputBy || Form._private.Adult1OutputBy;

    Form.Adult1WorkingTime = !Form.Adult1Employed || (Form.Output === "WorkingTime" && Form.Adult1OutputBy === "workdayp")
        ? null
        : Form.Adult1WorkingTime || Form._private.Adult1WorkingTime;

    Form.UnemploymentBenefit = Form.UnemploymentBenefit === null ? !Form.Adult1Employed : Form.UnemploymentBenefit;
    Form.AloneParentBenefit = !Form.Couple && Form.Children > 0 ? Form.AloneParentBenefit || false : null;
    Form.HomecareBenefit = Form.Children > 0 ? Form.HomecareBenefit || true : null;

    var showChildcareBenefit = Form.Children > 0 &&
        Form.ChildrenAges.every(function (age) { return age < 5 }) &&
        Form.Adult1Employed === true && Form.Adult1WorkingTime == 100 &&
        (!Form.Couple || (Form.Adult2Employed === true && Form.Adult2WorkingTime == 100));

    Form.ChildcareBenefit = showChildcareBenefit ? Form.ChildcareBenefit || false : null;

    if (Form.Adult1Amount == null && Form.Adult2Amount == null && Form.Adult1PreviousAmount == null && Form.HousingAmount == null)
    {
        Form.AmountCountry = null;
    }

    if (Form.Adult1Employed) 
    {
        if (!Form.NewJobBenefit) 
        {
            Form.MonthsOutOfWork = null;
            Form.CurrentJob = null;
        }
    }
    else 
    {
        Form.Adult1WorkingTime = null;
    }

    if (!Form.Adult1Employed
        && (Form.SocialContribution === null || Form.SocialContribution > (Form.AdultsAge - 18) * 12))
        Form.SocialContribution = (Form.AdultsAge - 18) * 12;
    else if (Form.Adult1Employed) Form.SocialContribution = null;

    $("#form-summary", context).html(renderForm());
    $("#wizard-run-button,#wizard-next-button").hide();
    $("#wizard-reset-button").css("visibility", "hidden");

    if (!context.data("wired")) {
        $("#summary-run-button", context).on("click", _launchTaxben);
        $(".wizard-path-step.last").on("click", function () {
            if (Wizard.wizard("index") === 6) {
                _launchTaxben();
            }
        });

        function _launchTaxben() {
            $(".taxben-result-button-container-top").empty();
            var Form_copy = $.extend($(), Form);
            delete (Form_copy._private);
            $("#hdnTaxbenFormJson").val(JSON.stringify(Form_copy));
            RetryCounter = 0;
            makeChart(Form_copy);
        }

        $("#summary-reset-button", context).on("click", resetTaxbenForm);
    }
    context.data("wired", true);
}

function renderForm() {

    var render = $("<table>"), val, det;

    for (var key in Labels) {

        var elts = Labels[key](Form[key]);

        render.append($("<tr>").append($("<td>", { text: elts[1] }))
            .append($("<td>").append($("<a>", {
                href: "#",
                html: elts[2],
                "data-taxben-index": elts[0],
                title: elts[0] > 0 ? "Modify " + elts[1] : null,
                class: elts[0] < 0 ? "nofollow" : null,
                on: {
                    click: elts[0] < 0 ? null : function () {
                        var index = parseInt($(this).attr("data-taxben-index"));
                        $(".wizard-main").wizard("index", index);
                    }
                }
            })).append(elts[3] ? elts[3].info() : $())));
    }

    return render;
}

function invokeWarning(allowContinue, msg, callback) {

    $('#dialog').dialog({
        title: "Warning",
        modal: true,
        position: { my: "center", at: "center", of: ".wizard-steps-container" },
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", this.parentNode).hide();
            $(this).html(msg);
        },
        close: function (event, ui) {
            if (allowContinue)  callback();
            else                Wizard.wizard("index", 2);
            $(this).dialog('destroy');
        },
        buttons: [{
            text: allowContinue ? "Continue" : "Edit",
            click: function () {
                $(this).dialog("close");
            }
        }]
    });
}

jQuery.fn.ForceNumericOnly = function () {
        return this.each(function () {
            $(this).keydown(function (e) {
                var key = e.charCode || e.keyCode || 0;
                // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
                // home, end, period, and numpad decimal
                return (
                    key == 8 ||
                    key == 9 ||
                    key == 13 ||
                    key == 46 ||
                    key == 110 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) || // 1 - 9
                    (key >= 96 && key <= 105));
            });
        });
};

function checkInputNotEmpty($input) {
    var value = $input.val();
    $input.toggleClass('error', isEmpty(value));
    return !isEmpty(value)
}