﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Script.Serialization;

public class TaxbenHandlerOld : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    private System.Web.Script.Serialization.JavaScriptSerializer _jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
    private TaxbenOptions _options;
    private HttpContext _context;
    private string _guid;

    public void ProcessRequest(HttpContext context)
    {
        TaxbenCommandManager command = null;
        try
        {
            _context = context;
            context.Response.ContentType = "application/json";
            string serTaxbenOptions = context.Request.Form["options"];

            _options = _jsonSerializer.Deserialize<TaxbenOptions>(serTaxbenOptions);

            //command = new TaxbenCommandManager(_options, _options.Guid);
            //_guid = command.Id;
            //command.RunCommand(WriteResponse);

            WriteResponse("", "");
        }
        catch (System.Exception ex)
        {
            TaxbenResultOld result = new TaxbenResultOld();
            result.Guid = _guid;
            result.ExceptionMessage = string.Concat(ex.Message, ex.InnerException != null ? string.Concat("\n(", ex.InnerException.Message, ")") : string.Empty);
            context.Response.Write(_jsonSerializer.Serialize(result));
        }
    }

    private void WriteResponse(string folderURL, string cmd)
    {
        try
        {
            if (folderURL == null) throw new System.Exception("Target folder error");
            else
            {
                TaxbenResultOld result = new TaxbenResultOld(folderURL, _context.Request.MapPath(folderURL), _options);
                result.Guid = _guid;
                result.TaxbenCommand = cmd;
                _context.Response.Write(_jsonSerializer.Serialize(result));
            }
        }
        catch (System.Exception ex)
        {
            TaxbenResultOld result = new TaxbenResultOld();
            result.Guid = _guid;
            result.ExceptionMessage = string.Concat(ex.Message, ex.InnerException != null ? string.Concat("\n(", ex.InnerException.Message, ")") : string.Empty);
            _context.Response.Write(_jsonSerializer.Serialize(result));
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}

/// <summary>
/// Summary description for TaxbenResult
/// </summary>
public class TaxbenResultOld
{
    public TaxbenResultOld() { }
    public TaxbenResultOld(string _folderURL, string _folderPath, TaxbenOptions options)
    {
        _dataPath = string.Format(@"D:\Dev_my\TaxBen\_samples\{0}.csv", options.Output);  //Path.Combine(_folderPath, ConfigurationManager.AppSettings["TaxbenCSVFile"]);

        if (File.Exists(_dataPath))
        {
            RawData = GetData(options.Output);
        }
        else
        {
            RawData = new Dictionary<string, List<double>>();
        }
        string xls = Path.Combine(_folderPath, ConfigurationManager.AppSettings["TaxbenXLSFile"]);
        if (File.Exists(xls))
        {
            ExcelURL = string.Concat(_folderURL, "/", ConfigurationManager.AppSettings["TaxbenXLSFile"]);
        }
        string log = Path.Combine(_folderPath, ConfigurationManager.AppSettings["TaxbenLogFile"]);
        if (File.Exists(log))
        {
            LogURL = string.Concat(_folderURL, "/", ConfigurationManager.AppSettings["TaxbenLogFile"]);
        }
        Title = GetTitle(options);
        Subtitle = GetSubtitle(options);
    }

    [ScriptIgnore]
    private string _dataPath;
    [ScriptIgnore]
    public int? OrderOnIndex;
    [ScriptIgnore]
    public Dictionary<string, List<double>> RawData;
    [ScriptIgnore]
    private int _nbLines;
    [ScriptIgnore]
    private Dictionary<string, List<object>> _cols;
    [ScriptIgnore]
    private Dictionary<string, List<object>> Columns
    {
        get
        {
            if (_cols == null)
            {
                Dictionary<string, List<object>> dico = new Dictionary<string, List<object>>();
                char SEP = ConfigurationManager.AppSettings["CSVSeparator"][0];
                string[] lines = File.ReadAllLines(_dataPath), line;
                _nbLines = lines.Length;
                if (lines != null && lines.Where(l => !string.IsNullOrEmpty(l.Trim())).Count() > 1)
                {
                    foreach (string s in lines[0].Split(SEP))
                    {
                        if (lines.Length < 2 && lines[0].Split(SEP).Length < 2)
                        {
                            return dico;
                        }
                        dico.Add(s, new List<object>());
                    }
                    if (lines.Length > 1)
                    {
                        for (int i = 1; i < lines.Length; i++)
                        {
                            line = lines[i].Split(SEP);
                            for (int j = 0; j < line.Length; j++)
                            {
                                dico[dico.Keys.ToList()[j]].Add((object)line[j]);
                            }
                        }
                    }
                }
                _cols = dico;
            }
            return _cols;
        }
    }

    public string Guid { get; set; }

    public string ExcelURL { get; set; }
    public string LogURL { get; set; }

    public string Title { get; set; }
    public string Subtitle { get; set; }

    public List<string> OrderedCategories
    {
        get
        {
            if (RawData == null) return null;
            else
            {
                if (OrderOnIndex == null)
                {
                    if (RawData.All(kvp => { int n; return int.TryParse(kvp.Key, out n); }))
                        return RawData.OrderBy(kvp => int.Parse(kvp.Key)).Select(kvp => kvp.Key).ToList();
                    else
                        return RawData.OrderBy(kvp => kvp.Key).Select(kvp => kvp.Key).ToList();
                }
                else
                    return RawData.OrderBy(kvp => kvp.Value[(int)OrderOnIndex]).Select(kvp => kvp.Key).ToList();
            }
        }
    }
    public List<List<double>> OrderedSeries
    {
        get
        {
            if (RawData == null) return null;
            else
            {
                List<List<double>> r = new List<List<double>>();
                foreach (string c in OrderedCategories)
                {
                    for (int i = 0; i < RawData[c].Count; i++)
                    {
                        if (r.Count <= i) r.Add(new List<double>());
                        r[i].Add(RawData[c][i]);
                    }
                }
                return r;
            }
        }
    }

    public string TaxbenCommand { get; set; }
    public string ExceptionMessage { get; set; }

    private string GetSubtitle(TaxbenOptions options)
    {
        string subtitle = options.Couple ? "Couple" : "Single adult";
        subtitle = string.Concat(subtitle, " with ");
        if (options.Children == 0)
        {
            subtitle = string.Concat(subtitle, "no children");
        }
        else if (options.Children == 1)
        {
            subtitle = string.Concat(subtitle, "one child");
        }
        else
        {
            subtitle = string.Concat(subtitle, options.Children, " children");
        }
        return subtitle;
    }
    private string GetTitle(TaxbenOptions options)
    {
        return string.Format(ConfigurationManager.AppSettings["ChartTitle" + options.Output.ToString()],
                options.CountryName, options.Years[0]);
    }

    private Dictionary<string, List<double>> GetData(TaxbenOptions.OutputBy output)
    {
        var data = new Dictionary<string, List<double>>();

        try
        {
            string cat;
            List<object> cats = new List<object>();

            if (Columns.Count == 0)
            {
                return data;
            }

            switch (output)
            {
                case TaxbenOptions.OutputBy.Country:
                    OrderOnIndex = 0;
                    cats = Columns["country_name"];
                    break;

                case TaxbenOptions.OutputBy.Year:
                    OrderOnIndex = null;
                    cats = Columns["year"];
                    break;

                case TaxbenOptions.OutputBy.WorkingTime:
                    OrderOnIndex = null;
                    cats = Columns["hours_work"];
                    break;

                case TaxbenOptions.OutputBy.Unemployment:
                    OrderOnIndex = null;
                    cats = Columns["time_no_job"];
                    break;

                default:
                    break;
            }

            for (int ri = 0; ri < _nbLines - 1; ri++)
            {
                cat = cats[ri].ToString();
                data.Add(cat, new List<double>());
                data[cat].Add(Percent(ri, "net", "aw"));
                data[cat].Add(-Percent(ri, "it", "aw"));
                data[cat].Add(-Percent(ri, "sc", "aw"));
                data[cat].Add(Percent(ri, "iw", "aw"));
                data[cat].Add(Percent(ri, "fb", "aw"));
                data[cat].Add(Percent(ri, "hb", "aw"));
                data[cat].Add(Percent(ri, "ub", "aw"));
                data[cat].Add(Percent(ri, "sa", "aw"));
                data[cat].Add(Percent(ri, "gross", "aw"));
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error reading from CSV", ex);
        }
        return data;
    }

    private double Percent(int i, string col, string total)
    {
        try
        {
            return Math.Round(CDbl(Columns[col][i]) / CDbl(Columns[total][i]) * (double)100, 2);
        }
        catch (Exception ex)
        {
            throw new Exception(string.Format("A calculation could not be made because of an erroneous value in CSV file at line {0}, column {1}: {2}", i, col, ex.Message));
        }
    }
    private double CDbl(object d)
    {
        return double.Parse(d.ToString(), System.Globalization.CultureInfo.InvariantCulture);
    }

}