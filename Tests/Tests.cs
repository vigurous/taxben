﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class Tests
    {
        [TestCase(TaxbenOptions.OutputBy.Country, false)]
        [TestCase(TaxbenOptions.OutputBy.Country, true)]
        [TestCase(TaxbenOptions.OutputBy.Year, false)]
        [TestCase(TaxbenOptions.OutputBy.WorkingTime, false)]
        [TestCase(TaxbenOptions.OutputBy.Unemployment, false)]
        public void Test(TaxbenOptions.OutputBy output, bool childcare)
        {
            var csvFilename = $@"D:\Work\Dev_my\TaxBen\_samples\{output}.new.csv";

            var options = new TaxbenOptions()
            {
                Output = output,
                Years = new List<int>() { 2021},
                CountryName = "Estonia",
                Countries = new List<string>() { "Estonia"},
                ChildcareBenefit = childcare
            };

            var result = StataCsvOutputProcessor.GetOuputData(
                options, 
                null, 
                null, 
                "",
                "", 
                csvFilename
            );

            Console.WriteLine(TaxbenHandler.Serialize(result));
        }
    }
}
